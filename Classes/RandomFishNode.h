//
//  RandomFishNode.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/7/17.
//
//

#ifndef RandomFishNode_h
#define RandomFishNode_h
#include "GameData.h"
#include "ParticleHelper.h"
class RandomFishNode: public Node {

public:
    RandomFishNode(){};
    ~RandomFishNode(){};
    
    static RandomFishNode* create(Node*);
    
    void initOptions();
    
    void respawnFish();

};

#endif /* RandomFishNode_h */
