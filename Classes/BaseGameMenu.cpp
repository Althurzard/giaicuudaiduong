//
//  BaseGameMenu.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/1/17.
//
//



#include "GameMode.h"
#include "GameShop.h"
#include "GameLevel.h"
#include "GameMissionInfo.h"
#include "GameMenuTutorial.h"
void BaseGameMenu::goToPage(Node* currentPage, const Page& goToPage, const float& delay){
    auto wait = DelayTime::create(delay);
    auto callback = CallFunc::create([&,goToPage,currentPage](){
        BaseGameMenu* nextMenu;
        switch (goToPage) {
            case Page::Shop:
                nextMenu = GameShop::create(currentPage->getParent());
                break;
            case Page::SelectMode:
                nextMenu = GameMode::create(currentPage->getParent());
                break;
            case Page::MissionInfo:
                nextMenu = GameMissionInfo::create(currentPage->getParent());
                break;
            case Page::Menu:
                if (GameData::isPlayTutorial) {
                    nextMenu = GameMenu::create(currentPage->getParent());
                } else {
                    nextMenu = GameMenuTutorial::create(currentPage->getParent());
                }
                break;
            case Page::SelectLevel:
                nextMenu = GameLevel::create(currentPage->getParent());
        }
    });
    auto removeSelf = RemoveSelf::create();
    currentPage->runAction(Sequence::create(wait,callback,removeSelf,NULL));
}

void BaseGameMenu::initOptions(){
    _backBtn = BubbleButton::create(this, 1.0, fileName("Back"), 0.3);
    _backBtn->setPosition(_backBtn->getBoundingBox().size.width / 2 + 15, visibleResolutionSize.height - _backBtn->getBoundingBox().size.height/2 - 15 );
    _backBtn->setOpacity(0);
    btnArray.pushBack(_backBtn);
}

void BaseGameMenu::hide(){
    for (auto btn : btnArray){
        GameHelper::scaleAniamtion(btn,
                                   btn->getScaleX()+0.2,
                                   btn->getScaleY()+0.2,
                                   0, 0, 0.2);
    }
}
