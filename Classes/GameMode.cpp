//
//  GameMode.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/3/17.
//
//

#include "GameMode.h"
#include "BaseTutorialScene.h"
GameMode::GameMode(){
  
}

GameMode::~GameMode(){
    
}

GameMode* GameMode::create(Node* layer){
    auto ret = new GameMode;
    if (ret->init()) {
        ret->autorelease();
        ret->initOptions();
        ret->addEvent();
        layer->addChild(ret,10);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
}

void GameMode::initOptions(){
    BaseGameMenu::initOptions();
    
    _freeBtn = BubbleButton::create(this, 2.2, fileName("FreeMode"), 0.15);
    _freeBtn->setPosition(centerScene.x, centerScene.y + _freeBtn->getBoundingBox().size.height * 0.55);
    _freeBtn->setOpacity(0);
    btnArray.pushBack(_freeBtn);
    
    _challengeBtn = BubbleButton::create(this, 2.2, fileName("ChallengeMode"), 0.15);
    _challengeBtn->setPosition(centerScene.x,centerScene.y - _challengeBtn->getBoundingBox().size.width * 0.55);
    _challengeBtn->setOpacity(0);
    btnArray.pushBack(_challengeBtn);
    
    
    auto Sequence = GameHelper::squenceScaleInOut(btnArray, 0.3f);
    
    this->runAction(Sequence);
    
}

void GameMode::addEvent(){
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = [&](cocos2d::Touch* touch, cocos2d::Event* event){
        
        auto realPos = this->convertTouchToNodeSpace(touch);
        auto backRect = this->_backBtn->getBoundingBox();
        auto challengeRect = this->_challengeBtn->getBoundingBox();
        auto freeRect = this->_freeBtn->getBoundingBox();
        
        if (backRect.containsPoint(realPos)) {
            //back to menu
            this->_backBtn->Explode();
            this->btnArray.eraseObject(this->_backBtn);
            this->hide();
            BaseGameMenu::goToPage(this, Page::Menu);
            return true;
        } else if (challengeRect.containsPoint(realPos)) {
            //go to game level select
            GameData::Mode = MODE::CHALLENGE;
            this->_challengeBtn->Explode();
            this->btnArray.eraseObject(this->_challengeBtn);
            this->hide();
            BaseGameMenu::goToPage(this, Page::SelectLevel);
            return true;
        } else if (freeRect.containsPoint(realPos)) {
            //go to main game - free mode
            GameData::Mode = MODE::FREE;
            this->_freeBtn->Explode();
            this->btnArray.eraseObject(this->_freeBtn);
            this->hide();
            auto wait = DelayTime::create(0.3f);
            auto callback = CallFunc::create([&](){
                
                Scene* scene;
                if (GameData::isPlayTutorial) {
                    scene = BaseTutorialScene::createScene();
                } else {
                    scene = BaseGameScene::createScene();
                }
                
                Director::getInstance()->replaceScene(TransitionFade::create(0.5, scene));
            });
            this->runAction(Sequence::create(wait,callback, NULL));
            return true;
        }
        return false;
    };
    
    cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
}

void GameMode::hide(){
    BaseGameMenu::hide();
}
