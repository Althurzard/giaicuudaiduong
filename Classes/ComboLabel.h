//
//  ComboLabel.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/27/16.
//
//

#ifndef ComboLabel_h
#define ComboLabel_h
#include "GameData.h"
#define _COMBO_NAME_ "Combo Label"
class ComboLabel: public Label {
    
public:
    ComboLabel();
    ~ComboLabel();
    
    static ComboLabel* create(Node*,const Vec2&);
    
    void initOptions(const Vec2&);
};

#endif /* ComboLabel_h */
