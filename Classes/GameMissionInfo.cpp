//
//  GameMissionInfo.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/4/17.
//
//

#include "GameMissionInfo.h"
#include "BaseTutorialScene.h"
GameMissionInfo::GameMissionInfo(){
    
}

GameMissionInfo::~GameMissionInfo(){
    
}

GameMissionInfo* GameMissionInfo::create(Node* node){
    auto ret = new GameMissionInfo;
    if (ret->init()) {
        ret->autorelease();
        ret->initOptions();
        ret->addEvent();
        node->addChild(ret,10);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
}

void GameMissionInfo::initOptions(){
    BaseGameMenu::initOptions();
    
    auto missionBoard = MissionBoard::create(this);
    
    auto delay = 0.5f;
    auto moveTo = MoveTo::create(delay, Vec2(0,0));
    missionBoard->runAction(moveTo);
    
    _btnGo = BubbleButton::create(this, 1.6, fileName("Go"), 0.5);
    _btnGo->setPosition(centerScene.x,centerScene.y - visibleResolutionSize.height*0.2);
    _btnGo->setOpacity(0);
    btnArray.pushBack(_btnGo);
    
    auto sequence = GameHelper::squenceScaleInOut(btnArray, delay-0.1);
    
    this->runAction(sequence);
    
}

void GameMissionInfo::addEvent(){
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = [&](cocos2d::Touch* touch, cocos2d::Event* event){
        
        auto realPos = this->convertTouchToNodeSpace(touch);
        auto backRect = this->_backBtn->getBoundingBox();
        auto goRect = this->_btnGo->getBoundingBox();
        
        if (backRect.containsPoint(realPos)) {
            //back to menu
            this->_backBtn->Explode();
            this->btnArray.eraseObject(this->_backBtn);
            this->hide();
            BaseGameMenu::goToPage(this, Page::SelectLevel);
            return true;
        }
        if (goRect.containsPoint(realPos)) {
            this->_btnGo->Explode();
            Scene* scene;
            if (GameData::isPlayTutorial) {
                scene = BaseTutorialScene::createScene();
            } else {
                scene = BaseGameScene::createScene();
            }

            Director::getInstance()->replaceScene(TransitionFade::create(0.5, scene));
        }
        
        return false;
    };
    
    cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
}

void GameMissionInfo::hide(){
    BaseGameMenu::hide();
}
