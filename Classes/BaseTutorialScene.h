//
//  BaseTutorialScene.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/7/17.
//
//

#ifndef BaseTutorialScene_h
#define BaseTutorialScene_h
#include "GameHelper.h"
#include "MainGameBackground.h"
#include "HUDLayer.h"
#include "BaseGameScene.h"
enum Step {
    One,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight
};
class BaseTutorialScene : public BaseGameScene {
protected:
    
public:
    BaseTutorialScene();
    ~BaseTutorialScene();
    
    static Scene* createScene();
    
    
    virtual bool init();
    static void nextStep(const Step&);
    virtual void onTouchesEnded(const std::vector<Touch*> & touches, Event* event);
    void initBubbleNormal();
    void initSpecialBubble(const std::string& name);
    void initItems();
    void eliminateAllNormalBubble();
    void eliminateAllBubble();
    void stopGame();
    void startShooting(const Vec2& postion);
    bool onContactBegin(PhysicsContact& contact);
    Step currentStep;
    CREATE_FUNC(BaseTutorialScene);
};

#endif /* BaseTutorialScene_h */
