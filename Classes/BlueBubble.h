//
//  BlueBubble.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/29/16.
//
//

#ifndef BlueBubble_h
#define BlueBubble_h
#include "BubbleNormal.h"
#define SPECIAL_BLUE_BUBBLE_NAME "Special blue bubble"
class BlueBubble: public BubbleNormal {

public:
    BlueBubble();
    ~BlueBubble();
    
    static BlueBubble* create(Layer*);
    static BlueBubble* create(Node*);
    virtual void initOptions();
    virtual void addEvent();
    virtual void Explode();
    virtual void changeState(float dt);
};

#endif /* BlueBubble_h */
