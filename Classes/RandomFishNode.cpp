//
//  RandomFishNode.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/7/17.
//
//

#include "RandomFishNode.h"
#include "Item.h"

RandomFishNode* RandomFishNode::create(Node* scene){
    auto ret = new RandomFishNode;
    if (ret->init()) {
        ret->autorelease();
        ret->initOptions();
        scene->addChild(ret,3);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
}

void RandomFishNode::initOptions(){
    //recursive to respawn fish

    auto delay = DelayTime::create(1.5);
    auto callback = CallFunc::create([&](){
        this->respawnFish();
    });
    auto sequence = Sequence::create(delay,callback, NULL);
    this->runAction(RepeatForever::create(sequence));
}


void RandomFishNode::respawnFish()
{
    
    
    auto randomFish = cocos2d::random(0, (int)GameData::characterArr.size()-1);
    auto charname = GameData::characterArr.at(randomFish)->getName();
    
    
    //get texture
    int i;
    int maxTexture;

    if (charname == "elephant" || charname == "monkey" || charname == "zebra" || charname == "penguin") {
        i = 7;
        maxTexture = 12;
    } else {
        i = 1;
        maxTexture = 6;
    }

    
    char str[100] = {0};
    auto cache = SpriteFrameCache::getInstance();
    Vector<SpriteFrame*> _animation;
    for (; i <= maxTexture; i++) {
        sprintf(str, "%s_%d.png", charname.c_str(), i);
        _animation.pushBack(cache->getSpriteFrameByName(str));
    }
    
    //random left and right
    auto leftOrRight = random(0, 1);
    
    auto fish = Sprite::createWithSpriteFrame(_animation.at(0));
    auto posY = random(fish->getContentSize().height,visibleResolutionSize.height);
    auto posX = leftOrRight == 0 ? leftOrRight : visibleResolutionSize.width;
    fish->setPosition(posX, posY);
    fish->setScale(random(1.0, 3.3));
    
    auto animation = cocos2d::Animation::createWithSpriteFrames(_animation,0.18f);
    fish->runAction(cocos2d::RepeatForever::create(cocos2d::Animate::create(animation)));
    this->addChild(fish);
    
    //fish move
    ActionInterval* move;
    if (posX == visibleResolutionSize.width) {
        fish->setScaleX(fish->getScaleX()*-1);
        move = MoveTo::create(random(1.0, 5.0), Vec2(0,fish->getPositionY()));
    } else {
        move = MoveTo::create(random(1.0, 5.0), Vec2(visibleResolutionSize.width,fish->getPositionY()));
    }
    auto remove = RemoveSelf::create();
    auto sq = Sequence::create(move,remove, NULL);
    fish->runAction(sq);

}
