//
//  HUDLayer.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/27/16.
//
//

#include "HUDLayer.h"
#include "BaseGameScene.h"
#define _COMBO_TIMING_ 20

HUDLayer::HUDLayer(){
    _heart.reserve(3);
}


HUDLayer::~HUDLayer(){
    _heart.clear();
}

HUDLayer* HUDLayer::createScene(Scene* scene){
    auto ret = new HUDLayer;
    if (ret->init()) {
        ret->autorelease();
        ret->initOptions();
        scene->addChild(ret,10);
        return ret;
    }
    CC_SAFE_DELETE(ret);
    return nullptr;
}


void HUDLayer::initOptions(){
    

    this->setName(__HUD_NAME__);
    //background
    auto bg = Sprite::create(fileName("InfoBoard"));
    auto sX = visibleResolutionSize.width / bg->getContentSize().width;
    auto sY = visibleResolutionSize.height * 0.2 / bg->getContentSize().height;
    bg->setScale(sX, sY);
    bg->setPosition(visibleResolutionSize.width*0.5, visibleResolutionSize.height - bg->getBoundingBox().size.height / 2);
    this->addChild(bg,1);
    
    //bubble button
    _bubbleTimer = BubbleButton::create(this);
    _bubbleTimer->setScale(1.7, 1.7);
    _bubbleTimer->setPosition(bg->getBoundingBox().getMidX(), bg->getBoundingBox().getMidY());

    
    
    //Timer
    _timer = TimerLabel::create(this);
    _timer->setPosition(bg->getBoundingBox().getMidX(),bg->getBoundingBox().getMidY());
    GameData::TimerPosition = _timer->getPosition();
    
    auto paddingLeft = 100;
    auto paddingTop = 50;
    //count frog
    auto cache = SpriteFrameCache::getInstance();
    auto logo = Sprite::createWithSpriteFrame(cache->getSpriteFrameByName(GameData::characterName+"_1.png"));
    logo->setScale(1.2, 1.2);
    logo->setPosition(bg->getBoundingBox().getMinX() + paddingLeft, bg->getBoundingBox().getMaxY() - paddingTop);
    this->addChild(logo,2);
    GameData::FrogPosition = logo->getPosition();
    
    _countFrogs = Label::createWithTTF("0", fileName("Chalkboard"), 30);
    _countFrogs->setAlignment(TextHAlignment::RIGHT);
    _countFrogs->setPosition(logo->getPositionX() + paddingLeft/2,logo->getBoundingBox().getMidY());
    this->addChild(_countFrogs,3);
    
    //count death frog
    auto logo2 = Sprite::createWithSpriteFrame(cache->getSpriteFrameByName(GameData::characterName+"_death.png"));
    logo2->setScale(1.2);
    logo2->setPosition(Vec2(logo->getPositionX(),
                            logo->getBoundingBox().getMinY() - logo2->getBoundingBox().size.height*0.5));
    this->addChild(logo2,2);
    GameData::DeathFrogPosition = logo2->getPosition();
    
    _countDeathFrogs = Label::createWithTTF("0", fileName("Chalkboard"), 30);
    _countDeathFrogs->setAlignment(TextHAlignment::RIGHT);
    _countDeathFrogs->setPosition(_countFrogs->getPositionX(),logo2->getBoundingBox().getMidY());
    this->addChild(_countDeathFrogs,3);
    
    
    //heart
    for (int i = 0; i < GameData::Life; i++){
        auto life = Sprite::create(fileName("Heart"));
        life->setScale(0.2, 0.2);
        if (i == 0) {
            life->setPosition(logo ->getPositionX(), bg->getBoundingBox().getMinY() + paddingTop);
        } else {
            life->setPosition(_heart.at(i-1)->getPositionX() + paddingLeft/2 + life->getBoundingBox().size.width/2,
                              _heart.at(i-1)->getPositionY());
        }
        this->addChild(life,3);
        _heart.pushBack(life);
    }
    GameData::HeartPosition = _heart.at(2)->getPosition();
    
    
    
    auto scorelogo = Sprite::create(fileName("ScoreLogo"));
    scorelogo->setScale(0.3);
    scorelogo->setPosition(_bubbleTimer->getBoundingBox().getMaxX() + paddingLeft / 2  ,
                           logo->getPositionY());
    this->addChild(scorelogo,3);
    
    _score = Label::createWithTTF("0", fileName("Chalkboard"), 30);
    _score->setAlignment(TextHAlignment::RIGHT);
    _score->setPosition(scorelogo->getPositionX() + scorelogo->getBoundingBox().size.width * 0.7,
                        scorelogo->getBoundingBox().getMidY());
    this->addChild(_score,3);
    
    
    auto mcomboLogo = Sprite::create(fileName("MCombo"));
    mcomboLogo->setScale(0.3);
    mcomboLogo->setPosition(_bubbleTimer->getBoundingBox().getMaxX() + paddingLeft / 2 + 10,
                            bg->getBoundingBox().getMinY() + paddingTop);
    this->addChild(mcomboLogo,3);
    
    _mCombo = Label::createWithTTF("0", fileName("Chalkboard"), 30);
    _mCombo->setAlignment(TextHAlignment::RIGHT);
    _mCombo->setPosition(mcomboLogo->getPositionX() + mcomboLogo->getBoundingBox().size.width * 0.7,
                         mcomboLogo->getBoundingBox().getMidY() + 10);
    this->addChild(_mCombo,3);
    
    //pausebtn
    auto padding = 15;
    _pauseBtn = Sprite::create(fileName("PauseButton"));
    _pauseBtn->setScale(0.35);
    _pauseBtn->setPosition(visibleResolutionSize.width -
                           _pauseBtn->getBoundingBox().size.width/2 - padding,
                           _pauseBtn->getBoundingBox().size.height/2 + padding);
    this->addChild(_pauseBtn,10);

    
    
    //if play tutorial, we dont update game state
    if (GameData::isPlayTutorial) {
        return;
    }
    
    //if challenge mode, we need to update state to check if player complete mission
    switch (GameData::Mode) {
        case FREE:
            
            break;
        case CHALLENGE:
            currentLevel = GameData::levelDataArray.at(GameData::selectedLevel);
            scheduleUpdate();
            break;
    }
    
    addEvent();
}

void HUDLayer::addEvent(){
    listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = [&](cocos2d::Touch* touch, cocos2d::Event* event){
        
        
        
        if (this->_pauseBtn != nullptr) {
            auto pauseRect = this->_pauseBtn->getBoundingBox();
            
            if (pauseRect.containsPoint(touch->getLocation())) {
                PauseLayer::create(Director::getInstance()->getRunningScene());
                return true;
            }
        }
        
        if (this->_bubbleTimer != nullptr) {
            auto timerRect = this->_bubbleTimer->getBoundingBox();
            if (timerRect.containsPoint(touch->getLocation())) {
                this->endGame(GameResult::Lose);
                this->_bubbleTimer->Explode();
                this->_bubbleTimer = nullptr;
                return true;
            }
        }
        
        
        return false;
    };
    
    cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(listener, -128);
}


void HUDLayer::update(float dt){
    
    if (GameData::currentCombo >= currentLevel->getCombo() &&
        GameData::caught >= currentLevel->getFrogs()) {
        endGame(GameResult::Win);
    } else if (_timer->isTimeOver()){
        endGame(GameResult::Lose);
        this->_bubbleTimer->Explode();
    }
    
}

void HUDLayer::updateHeart(int life){

    if (GameData::GameOver || GameData::isPlayTutorial) {
        return;
    }
    GameData::Life += life;
    
    if (GameData::Life <= 0) {
        //end game
        _timer->stopUpdate();
        endGame(GameResult::Lose);
        return;
    }
    
    if (GameData::Life > MAX_LIFE) {
        GameData::Life = MAX_LIFE;
        return;
    }


        if (life < 0) {
            auto size = _heart.size()-1;
            GameData::HeartPosition = _heart.at(size)->getPosition();
            this->removeChild(_heart.at(size));
            this->_heart.popBack();
        } else {
            auto paddingLeft = 50;
            auto life = Sprite::create(fileName("Heart"));
            life->setScale(0.2, 0.2);
            life->setPosition(_heart.at(_heart.size()-1)->getPositionX() + paddingLeft +life->getBoundingBox().size.width/2,
                              _heart.at(_heart.size()-1)->getPositionY());
            this->addChild(life,10);
            _heart.pushBack(life);
        }
  
  
}

void HUDLayer::updateMCombo(){
    if (GameData::GameOver) {
        return;
    }
    GameData::currentCombo = GameData::tempCombo;
    this->_mCombo->setString(std::to_string(GameData::currentCombo));
}

void HUDLayer::updateScore(){
    if (GameData::GameOver) {
        return;
    }
   GameData::Score +=  3 + (0.2* GameData::tempCombo);
    this->_score->setString(std::to_string(GameData::Score));
}

void HUDLayer::updateCountFrogs(){
    if (GameData::GameOver) {
        return;
    }
    GameData::caught++;
    this->_countFrogs->setString(std::to_string(GameData::caught));
}

void HUDLayer::updateCountDeathFrogs(){
    if (GameData::GameOver) {
        return;
    }
    GameData::fishDeath++;
    this->_countDeathFrogs->setString(std::to_string(GameData::fishDeath));
}

void HUDLayer::startTimingCombo(){
    //in 2s if player cant keep tapping combo or tap miss, then should reset combo
    if (getActionByTag(_COMBO_TIMING_)) {
        stopActionByTag(_COMBO_TIMING_);
    }
    auto wait = DelayTime::create(2.0);
    auto lambda = CallFunc::create([&](){
        GameData::tempCombo = 0;
    });
    auto sequnce = Sequence::create(wait,lambda, NULL);
    sequnce->setTag(_COMBO_TIMING_);
    this->runAction(sequnce);
}

void HUDLayer::endGame(const GameResult& result){
    this->unscheduleUpdate();
    this->unscheduleAllCallbacks();
    GameData::GameOver = true;
    auto gameScene = (BaseGameScene*)_gameScene;
    gameScene->stopGame();
    GameHelper::pauseRecursive(this, true);
    this->_pauseBtn->removeFromParent();
    this->_pauseBtn = nullptr;
    auto scene = Director::getInstance()->getRunningScene();
    ResultLayer::create(scene,result);
    
      Director::getInstance()->getEventDispatcher()->removeEventListener(listener);
}

void HUDLayer::setCurrentGameScene(Node* node){
    this->_gameScene = node;
}
