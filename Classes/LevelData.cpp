//
//  LevelData.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/31/16.
//
//

#include "LevelData.h"
#include "GameData.h"
#define LEVEL_NAME "Level"

LevelData::LevelData(const int& frog,
                           const int& time,
                           const int& combo ,
                           const bool& unlock ,
                           const std::string& achievement){
    this->numberOfFrog = frog;
    this->time = time;
    this->combo = combo;
    this->unlock = unlock;
    this->achievement = achievement;
    
}

LevelData::LevelData(const int& level, const Document& data){
    
    this->name = LEVEL_NAME + std::to_string(level);

 
    if (level == 1) {
        this->unlock = true;
    } else {
        this->unlock = UserDefault::getInstance()->getBoolForKey(name.c_str(), false);
    }
    
    this->achievement = data[name.c_str()]["achievement"].GetString();
    this->combo = data[name.c_str()]["combo"].GetInt();
    this->numberOfFrog = data[name.c_str()]["animal"].GetInt();
    this->time = data[name.c_str()]["time"].GetInt();
    this->pathSprite = data[name.c_str()]["filepath"].GetString();
    this->respawnTime = data[name.c_str()]["respawntime"].GetDouble();
    
    
    
    switch (level) {
        case 1:
        {
            this->position = Vec2(centerScene.x - visibleResolutionSize.width*0.3,
                                  centerScene.y + visibleResolutionSize.height*0.2);
            break;
        }
        case 2:
        {
            this->position = Vec2(centerScene.x,centerScene.y + visibleResolutionSize.height*0.2);
            break;
        }
        case 3:
        {
            this->position = Vec2(centerScene.x + visibleResolutionSize.width*0.3,
                                  centerScene.y + visibleResolutionSize.height*0.2);
            break;
        }
        case 4:
        {
            this->position = Vec2(centerScene.x - visibleResolutionSize.width*0.3,
                                  centerScene.y);
            break;
        }
        case 5:
        {
            this->position = Vec2(centerScene);
            break;
        }
        case 6:
        {
            this->position = Vec2(centerScene.x + visibleResolutionSize.width*0.3,
                                  centerScene.y);
            break;
        }
        case 7:
        {
            this->position = Vec2(centerScene.x - visibleResolutionSize.width*0.3,
                                  centerScene.y - visibleResolutionSize.height*0.2);
            break;
        }
        case 8:
        {
            this->position = Vec2(centerScene.x,centerScene.y - visibleResolutionSize.height*0.2);
            break;
        }
        case 9:
        {
            this->position = Vec2(centerScene.x + visibleResolutionSize.width*0.3,
                                  centerScene.y - visibleResolutionSize.height*0.2);
            break;
        }
        default:
            break;
    }
    
}

LevelData::~LevelData(){
    
}

double LevelData::getRespawnTime(){
    return respawnTime;
}

int LevelData::getTime() {
    return time;
}

int LevelData::getCombo() {
    return combo;
}

int LevelData::getFrogs() {
    return numberOfFrog;
}

Vec2 LevelData::getPosition(){
    return position;
}

std::string LevelData::getPath(){
    return pathSprite;
}

void LevelData::setUnlock(bool unlock){
    cocos2d::log("Unlocked %s",name.c_str());
    this->unlock = unlock;
    UserDefault::getInstance()->setBoolForKey(this->name.c_str(), unlock);
    UserDefault::getInstance()->flush();
}

bool LevelData::isUnlock(){
    return unlock;
}

std::string LevelData::getAchievement(){
    return achievement;
}

LevelData& LevelData::operator=(const LevelData& other){
    
    this->numberOfFrog = other.numberOfFrog;
    this->time = other.time;
    this->unlock = other.unlock;
    this->achievement = other.achievement;
    this->combo = other.combo;
    this->position = other.position;
    this->pathSprite = other.pathSprite;
    return *this;
}
