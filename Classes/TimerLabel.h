//
//  TimerLabel.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/3/17.
//
//

#ifndef TimerLabel_h
#define TimerLabel_h
#include "GameData.h"

class TimerLabel: public Label {
private:
    bool timeOver;
public:
    TimerLabel();
    ~TimerLabel();
    
    static TimerLabel* create(Node*);
    bool isTimeOver();
    void initOptions();
    void updateTime(float dt);
    void stopUpdate();
};

#endif /* TimerLabel_h */
