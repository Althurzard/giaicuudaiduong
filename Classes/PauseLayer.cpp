//
//  PauseLayer.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/6/17.
//
//

#include "PauseLayer.h"
#include "BFAudioPlayer.h"
PauseLayer::PauseLayer(){
    
}

PauseLayer::~PauseLayer(){
    
}

PauseLayer* PauseLayer::create(Scene* scene){
    auto ret = new PauseLayer;
    if (ret->init()) {
        ret->autorelease();
        ret->initOption();
        ret->addEvent();
        scene->addChild(ret,100);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
}

void PauseLayer::initOption(){
    PauseLayer::pauseGame(true);
    
    _resumeBtn = Sprite::create(fileName("ResumeButton"));
    _resumeBtn->setPosition(centerScene);
    _resumeBtn->setScale(1.5);
    this->addChild(_resumeBtn);
}

void PauseLayer::addEvent(){
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = [&](cocos2d::Touch* touch, cocos2d::Event* event){
        
        auto resumeRect = this->_resumeBtn->getBoundingBox();
        if (resumeRect.containsPoint(touch->getLocation())) {
            PauseLayer::pauseGame(false);
            this->removeFromParent();
        }
        
        return false;
    };
    
    cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
}

void PauseLayer::pauseGame(const bool& isPause){
    
    auto runningScene = Director::getInstance()->getRunningScene();
    auto gameScene = runningScene->getChildByName(__GAME_SCENE__);
    auto HUDLayer = runningScene->getChildByName(__HUD_NAME__);
    
    if (isPause) {
        Director::getInstance()->getRunningScene()->getPhysicsWorld()->setSpeed(0);
        BFAudioPlayer::pauseBackgroundMusic();
    } else {
        BFAudioPlayer::resumeBackgroundMusic();
        Director::getInstance()->getRunningScene()->getPhysicsWorld()->setSpeed(1);
    }
    
    GameHelper::pauseRecursive(gameScene, isPause);
    GameHelper::pauseRecursive(HUDLayer,isPause);
}
