//
//  LevelData.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/31/16.
//
//

#ifndef LevelData_h
#define LevelData_h
#include "external/json/document.h"
#include "external/json/rapidjson.h"
#include "cocos2d.h"

class LevelData {
private:
    
    std::string name;
    std::string pathSprite;
    int numberOfFrog;
    int time;
    int combo;
    bool unlock;
    std::string achievement;
    double respawnTime;
    cocos2d::Vec2 position;
public:
    LevelData(const int&,const rapidjson::Document&);
    LevelData(const int&,
                 const int&,
                 const int&,
                 const bool&,
                 const std::string&);
    ~LevelData();
    
    int getFrogs();
    int getTime();
    int getCombo();
    double getRespawnTime();
    cocos2d::Vec2 getPosition();
    std::string getPath();
    
    void setUnlock(bool);
    bool isUnlock();
    std::string getAchievement();
    
    LevelData& operator=(const LevelData& other);
};

#endif /* LevelData_h */
