//
//  Templete.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/8/17.
//
//

#ifndef Templete_h
#define Templete_h

#include <string>
#include <sstream>

using namespace std;

namespace std
{
    template < typename T > std::string to_string( const T& n )
    {
        std::ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }
}

#endif  // Templates_h

