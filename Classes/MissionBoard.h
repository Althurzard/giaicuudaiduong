//
//  MissionBoard.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/4/17.
//
//

#ifndef MissionBoard_h
#define MissionBoard_h
#include "GameData.h"

class MissionBoard: public Node {
    
public:
    MissionBoard();
    ~MissionBoard();
    
    static MissionBoard* create(Node*);
    
    void initOptions();
};

#endif /* MissionBoard_h */
