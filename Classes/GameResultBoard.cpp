//
//  GameResultBoard.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/30/16.
//
//

#include "GameResultBoard.h"


GameResultBoard::GameResultBoard(){
    
}

GameResultBoard::~GameResultBoard(){
    
}

GameResultBoard* GameResultBoard::create(Node* node,
                                         const GameResult& Result){
    auto ret = new GameResultBoard;
    if (ret->init()) {
        ret->autorelease();
        ret->initOptions(Result);
        node->addChild(ret,10);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
}

void GameResultBoard::initOptions(const GameResult& Result){
    this->setPosition(0, visibleResolutionSize.height *0.35);
    auto postionLeft = Vec2(visibleResolutionSize.width*0.25,visibleResolutionSize.height*0.65);
    auto positionRight = Vec2(visibleResolutionSize.width*0.75,visibleResolutionSize.height*0.65);
    auto bubble = createBubble(postionLeft);
    createBubble(positionRight);
    
    Sprite* resultLogo;
    switch (Result) {
        case Win:
            resultLogo = Sprite::create(fileName("Victory"));
            break;
        case Lose:
            resultLogo = Sprite::create(fileName("GameOver"));
            break;
    }
    resultLogo->setScale(0.8);
    resultLogo->setPosition(visibleResolutionSize.width*0.5,bubble->getBoundingBox().getMaxY() + resultLogo->getContentSize().height *0.3);
    this->addChild(resultLogo);

}

Sprite* GameResultBoard::createBubble(const Vec2& position){
    auto bubble = Sprite::create(fileName("BubbleNormal"));
    bubble->setScale(0.7);
    bubble->setPosition(position);
    this->addChild(bubble);
    Item::create(bubble, GameData::characterName);
    return bubble;
}
