//
//  MainGameBackground.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/27/16.
//
//

#ifndef MainGameBackground_h
#define MainGameBackground_h
#include "GameData.h"

class MainGameBackground: public Sprite {
    
public:
    MainGameBackground();
    ~MainGameBackground();
    
    static MainGameBackground* create(Layer*);
    
    void initOptions();
    void addEvent();
};

#endif /* MainGameBackground_h */
