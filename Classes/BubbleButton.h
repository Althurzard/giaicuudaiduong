//
//  BubbleButton.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/30/16.
//
//

#ifndef BubbleButton_h
#define BubbleButton_h
#include "Bubble.h"
#include "ParticleHelper.h"
USING_NS_CC;
class BubbleButton: public Bubble {
    
public:
    BubbleButton();
    ~BubbleButton();
    
    static BubbleButton* create(Node*,
                                const float& bubbleScale,
                                const std::string& childFontName,
                                const float& childFontScale);
    static BubbleButton* create(Node*);
    
    void initOptions();
    void addEvent();
    void initOptions(const float& bubbleScale,
                     const std::string& childFontName,
                     const float& childFontScale);
    virtual void Explode();
    
};

#endif /* BubbleButton_h */
