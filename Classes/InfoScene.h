//
//  InfoScene.h
//  AngryRedHood
//
//  Created by Nguyen Quoc Vuong on 12/21/16.
//
//

#ifndef InfoScene_h
#define InfoScene_h
#include "GameHelper.h"

class InfoScene: public cocos2d::Layer {
private:
    cocos2d::Sprite* _board;
    cocos2d::Sprite* _yesBtn;
public:
    InfoScene(){};
    ~InfoScene(){};
    
    static InfoScene* create(cocos2d::Scene*,const std::string& boardName);
    
    void initOptions(const std::string& boardName);
    void addEvent();
};

#endif /* InfoScene_h */
