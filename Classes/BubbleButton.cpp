//
//  BubbleButton.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/30/16.
//
//

#include "BubbleButton.h"


BubbleButton::BubbleButton(){
    
}

BubbleButton::~BubbleButton(){
    
}

BubbleButton* BubbleButton::create(Node* node,
                                   const float& bubbleScale,
                                   const std::string& childFileName,
                                   const float& childScale){
    auto ret = new BubbleButton;
    if (ret->initWithFile(fileName("BubbleButton"))) {
        ret->autorelease();
        ret->initOptions(bubbleScale,childFileName, childScale);
        node->addChild(ret,10);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
}

BubbleButton* BubbleButton::create(Node* node){
    auto ret = new BubbleButton;
    if (ret->initWithFile(fileName("BubbleButton"))) {
        ret->autorelease();
        node->addChild(ret,10);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
}

void BubbleButton::initOptions(const float& bubbleScale,
                               const std::string& childFileName,
                               const float& childScale) {
    this->setScale(bubbleScale);
    this->setCascadeOpacityEnabled(true);
    auto font = Sprite::create(childFileName);
    font->setScale(childScale);
    font->setPosition(this->getContentSize().width*0.5,
                      this->getContentSize().height*0.5);
    this->addChild(font);
    
}

void BubbleButton::Explode(){
    BFAudioPlayer::playBubbleExplode();
    auto scene = Director::getInstance()->getRunningScene();
    if (scene->getChildByName(__GAMEMENU_NAME__)) {
        auto menuScene = scene->getChildByName(__GAMEMENU_NAME__);
        auto position = menuScene->convertToNodeSpace(this->getPosition());
        ParticleHelper::initParticle(menuScene, fileName("BubbleButtonExplode"), position,1.0);
        
    } else if (scene->getChildByName(__HUD_NAME__)) {
        cocos2d::log("tap button at HUD");
        auto gameScene = scene->getChildByName(__HUD_NAME__);
        auto position = gameScene->convertToNodeSpace(this->getPosition());
        ParticleHelper::initParticle(gameScene, fileName("BubbleButtonExplode"), position,1.0);
    }
    this->removeFromParent();
}

void BubbleButton::initOptions(){
    
}

void BubbleButton::addEvent(){
    
}

