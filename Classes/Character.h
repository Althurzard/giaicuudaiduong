//
//  Character.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/2/17.
//
//

#ifndef Character_h
#define Character_h
#include "cocos2d.h"
USING_NS_CC;


class Character {
private:
    std::string name;
    Vec2 position;
    bool unlock;
    bool selected;
public:
    Character();
    Character(const std::string& name);
    ~Character();
    
    
    
    //set
    void setUnlock(bool);
    void setSelect(bool);
    
    //get
    std::string getName();
    Vec2 getPosition();
    
    
    bool isUnlock();
    bool isSelected();
    
    
    
};


#endif /* Character_h */
