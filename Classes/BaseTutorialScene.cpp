//
//  BaseTutorialScene.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/7/17.
//
//

#include "BaseTutorialScene.h"
#include "StepTwo.h"
#include "StepThree.h"
#include "StepFour.h"
#include "StepFive.h"
#include "StepSix.h"
#include "StepSeven.h"
#include "StepEight.h"
#include "SDKHelper.h"
BaseTutorialScene::BaseTutorialScene(){
    
}

BaseTutorialScene::~BaseTutorialScene(){
    this->removeAllChildren();
}

Scene* BaseTutorialScene::createScene(){
    auto scene = Scene::createWithPhysics();
    
    auto layer = BaseTutorialScene::create();
    scene->getPhysicsWorld()->setGravity(Vec2(0,100));
    scene->addChild(layer);
    
    auto hud = HUDLayer::createScene(scene);
    hud->setCurrentGameScene(layer);
    //RandomFishNode::create(scene);
    
    
    
    return scene;
    
}

bool BaseTutorialScene::init(){
    
    if (!Layer::init()) {
        return false;
    }
    auto listener = EventListenerTouchAllAtOnce::create();
    listener->onTouchesEnded = CC_CALLBACK_2(BaseTutorialScene::onTouchesEnded, this);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
    
    auto contactListener = EventListenerPhysicsContact::create();
    contactListener->onContactBegin = CC_CALLBACK_1(BaseGameScene::onContactBegin, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(contactListener, this);
    
    this->setName(__TUTORIAL_SCENE__);
    
    //edge of the box
    auto edgeSp = Sprite::create();
    auto boundBody = PhysicsBody::createEdgeBox(visibleResolutionSize*1.1, PHYSICSBODY_MATERIAL_DEFAULT,1);
    edgeSp->setPosition(centerScene);
    edgeSp->setPhysicsBody(boundBody);
    this->addChild(edgeSp);
    edgeSp->setTag(PhysicBitmask::Wall);
    
    //background
    MainGameBackground::create(this);
    StepOne::create(this);
    BFAudioPlayer::playBackgroundMusic("sound/background.mp3");
    
    SDKHelper::Admob::hideBanner();
    
    auto touchListener = EventListenerKeyboard::create();
    touchListener->onKeyReleased = [](cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
    {
        if (keyCode == EventKeyboard::KeyCode::KEY_BACK) {
            Director::getInstance()->end();
        }
    };
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchListener, this);
    return true;
    
}

void BaseTutorialScene::onTouchesEnded(const std::vector<Touch*> & touches, Event* event){
    GameData::isTapObjects = false;
}

bool BaseTutorialScene::onContactBegin(PhysicsContact& contact){
    return BaseGameScene::onContactBegin(contact);
}

void BaseTutorialScene::stopGame(){
    getChildByTag(currentStep)->removeFromParent();
    BaseGameScene::stopGame();
    GameData::isPlayTutorial = false;
    GameData::saveData();
    
}

void BaseTutorialScene::nextStep(const Step& step){
    auto scene = (BaseTutorialScene*)Director::getInstance()->getRunningScene()->getChildByName(__TUTORIAL_SCENE__);
    switch (step) {
        case One:
            break;
        case Two:
            StepTwo::create(scene);
            break;
        case Three:
            StepThree::create(scene);
            break;
        case Four:
            StepFour::create(scene);
            break;
        case Five:
            scene->initBubbleNormal();
            scene->initSpecialBubble("Green");
            StepFive::create(scene);
            break;
        case Six:
            scene->stopAllActions();
            scene->initBubbleNormal();
            scene->initSpecialBubble("Blue");
            StepSix::create(scene);
            break;
        case Seven:
            scene->stopAllActions();
            scene->eliminateAllBubble();
            scene->initItems();
            StepSeven::create(scene);
            break;
        case Eight:
            scene->stopAllActions();
            StepEight::create(scene);
            break;
    }
    scene->currentStep = step;
}

void BaseTutorialScene::eliminateAllNormalBubble(){
    BaseGameScene::eliminateAllNormalBubble();
}
void BaseTutorialScene::eliminateAllBubble(){
    BaseGameScene::eliminateAllBubble();
}
void BaseTutorialScene::startShooting(const Vec2& postion){
    BaseGameScene::startShooting(postion);
}

void BaseTutorialScene::initBubbleNormal(){
    auto delay = DelayTime::create(0.9);
    auto callback = CallFunc::create([&](){
        BubbleNormal::create(this);
    });
    auto sequence = Sequence::create(callback,delay, NULL);
    auto repeat = RepeatForever::create(sequence);
    this->runAction(repeat);
}

void BaseTutorialScene::initSpecialBubble(const std::string& name){
    auto delay = DelayTime::create(10.0);
    auto callback = CallFunc::create([&,name](){
        if (this->getChildByTag(15)) {
            
            this->getChildByTag(15)->removeFromParent();
        }
        BubbleNormal* bubble;
        if (name == "Green") {
          bubble  = GreenBubble::create(this);
        } else {
          bubble  = BlueBubble::create(this);
        }
        bubble->setTag(15);
        bubble->setPosition(centerScene.x, visibleResolutionSize.height);
        bubble->getPhysicsBody()->removeFromWorld();
        
        auto move = MoveTo::create(0.5, centerScene);
        bubble->runAction(move);
    });
    auto sequence = Sequence::create(callback,delay, NULL);
    auto repeat = RepeatForever::create(sequence);
    this->runAction(repeat);
    
}

void BaseTutorialScene::initItems(){
    auto delay = DelayTime::create(10.0);
    auto callback = CallFunc::create([&](){
        //remove previous blue bubble
        if (this->getChildByTag(15)) {
            this->getChildByTag(15)->removeFromParent();
        }
        if (this->getChildByTag(20)) {
            return;
        }
        for (int i = 0; i < 2;i++) {
            BubbleNormal* bubble;
            if (i == 0) {
                bubble = BubbleFrog::create(this, "heart");
                bubble->setPosition(centerScene.x - bubble->getBoundingBox().size.width, visibleResolutionSize.height);
            } else {
                bubble = BubbleFrog::create(this, "timeflys");
                bubble->setPosition(centerScene.x + bubble->getBoundingBox().size.width, visibleResolutionSize.height);
            }
            bubble->setTag(20);
            
            bubble->getPhysicsBody()->removeFromWorld();
            
            auto move = MoveTo::create(0.5, Vec2(bubble->getPositionX(),centerScene.y));
            bubble->runAction(move);
        }
        
    });
    auto sequence = Sequence::create(callback,delay, NULL);
    auto repeat = RepeatForever::create(sequence);
    this->runAction(repeat);
}
