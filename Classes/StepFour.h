//
//  StepFour.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/7/17.
//
//

#ifndef StepFour_h
#define StepFour_h
#include "StepTwo.h"

class StepFour: public StepTwo {
private:
    int tempFrogs;
public:
    StepFour(){};
    ~StepFour(){};
    
    static StepFour* create(Node*);
    
    virtual void initOptions();
    virtual void addEvent();
    virtual void update(float dt);
    bool checkItem();
    
};

#endif /* StepFour_h */
