//
//  GameHelper.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/27/16.
//
//

#ifndef GameHelper_h
#define GameHelper_h
#include "GameData.h"

enum NumberAlignment {
    Left,Center,Right
};
class GameHelper {
    
public:
    
    static void setNormalNumber(cocos2d::Node*,
                         const cocos2d::Vec2& ,
                         const int&,
                         const float scale,
                         const NumberAlignment&); // normal number
    static void setSpeicalNumber(cocos2d::Node*,
                          const cocos2d::Vec2& ,
                          const int&,
                          const float scale,
                          const NumberAlignment&); // special number
    
    //for private use only
    static void leftAlignment(cocos2d::Node*,
                       const cocos2d::Vec2&,
                       cocos2d::Vector<cocos2d::Sprite*> numberArray);
    static void rightAlignment(cocos2d::Node*,
                        const cocos2d::Vec2&,
                        cocos2d::Vector<cocos2d::Sprite*> numberArray);
    static void centerAlignment(cocos2d::Node*,
                         const cocos2d::Vec2&,
                         cocos2d::Vector<cocos2d::Sprite*> numberArray);
    
    
    static void scaleAniamtion(cocos2d::Node*,
                        const float& maxx1,
                        const float& maxy1,
                        const float& minx2,
                        const float& miny2,
                        const float& duration);
    
    static Sequence* squenceScaleInOut(Vector<Node*> spriteArray,
                                       const float& delayTime,
                                       const float& extraScale = 0.2,
                                       const float& animateTime = 0.15,
                                       const float& opacity = 255);
    

    
    static void pauseRecursive(Node* _node, bool _pause);

    static float getDesiredRotationTowards(const Vec2& fromPosition,
                                    const Vec2& toPosition);
};

#endif /* GameHelper_h */
