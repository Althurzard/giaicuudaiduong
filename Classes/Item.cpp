//
//  Item.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/26/16.
//
//

#include "Item.h"
#include "GameData.h"
#include "HUDLayer.h"


Item::Item(){
    speed = 100;
    _isDeath = false;
}

Item::~Item(){

}

Item* Item::create(Node* node, const std::string& name){
    auto ret = new Item;
    auto cache = SpriteFrameCache::getInstance();
    if (ret->initWithSpriteFrame(cache->getSpriteFrameByName(name+"_1.png"))) {
        ret->autorelease();
        ret->initOptions(name);
        ret->setPosition(node->getContentSize().width *0.5, node->getContentSize().height*0.5);
        node->addChild(ret,1);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
}

void Item::initOptions(const std::string& name){
    
    this->setScale(2.5);
    this->setName(name);
    this->setCascadeOpacityEnabled(true);
    char str[100] = {0};
    auto cache = SpriteFrameCache::getInstance();
    for (int i = 1; i < numbersTexture(name.c_str()) ; i++) {
        sprintf(str, "%s_%d.png", name.c_str(), i);
        _animation.pushBack(cache->getSpriteFrameByName(str));
    }
    
    startAnimation();
}

void Item::startAnimation(){
    auto animation = cocos2d::Animation::createWithSpriteFrames(_animation,0.13f);
    this->runAction(cocos2d::RepeatForever::create(cocos2d::Animate::create(animation)));
}


void Item::addEvent(){
    auto listener = cocos2d::EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = [&](cocos2d::Touch *touch, cocos2d::Event *event){

        if (GameData::GameOver) {
            return false;
        }
        auto rect = this->getBoundingBox();
        if (rect.containsPoint(touch->getLocation())) {
            GameData::isTapObjects = true;
            auto HUD = (HUDLayer*)Director::getInstance()->getRunningScene()->getChildByName(__HUD_NAME__);
            for(auto charInfo : GameData::characterArr){
                if (this->getName() == charInfo->getName()) {
                    cocos2d::log("tap animal");
                    if (this->_isDeath) {
                        this->get(GameData::DeathFrogPosition);
                        HUD->updateCountDeathFrogs();
                    } else {
                        this->get(GameData::FrogPosition);
                        HUD->updateCountFrogs();
                    }
                    
                    return true;
                }
            }
            
            if (this->getName() == "heart"){
                this->get(GameData::HeartPosition);
                HUD->updateHeart(1);
            } else if (this->getName() == "timeflys") {
                this->get(GameData::TimerPosition);
                GameData::Time += random(5, 15);
            }
            return true;
        }
        return false;
    };
    
    listener->onTouchEnded = [=](cocos2d::Touch *touch, cocos2d::Event *event) {
        GameData::isTapObjects = false;
    };
    
    cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
}

void Item::get(const Vec2& endPosition){
    auto HUD = Director::getInstance()->getRunningScene()->getChildByName(__HUD_NAME__);
    auto tempPosition = this->getPosition();
    this->retain();
    this->removeFromParent();
    HUD->addChild(this,10);
    this->setPosition(tempPosition);
    this->release();
    
    if (!_isDeath) {
        startAnimation();
    }
    
    float scale = 0;
    if (this->getName() == "heart" || this->getName() == "timeflys") {
        scale = 0.2;
    } else {
        scale = 1.0;
    }
    auto moveTo = MoveTo::create(0.2, endPosition);
    auto scaleAnimation = ScaleTo::create(0.2, scale);
    auto remove = RemoveSelf::create();
    auto sequence = Sequence::create(moveTo,scaleAnimation,remove, NULL);
    this->runAction(sequence);
    
    
    
}

void Item::escape(const Vec2& position){
    
    
    
    if (!this->getReferenceCount()) {
        return;
    }
    
    this->retain();
    
    Node* layer;
    if (this->getParent() != NULL){
        //current Layer
        layer = this->getParent()->getParent();
        this->removeFromParent();
    }
    layer->addChild(this);
    this->setPosition(position);
    this->release();
    this->setOpacity(255);
 
    startAnimation();
    
    auto distance = this->getPosition().getDistance(Vec2(this->getPositionX(),visibleResolutionSize.height));
    auto time = distance / speed;
    
    
    auto move = MoveTo::create(time, Vec2(this->getPositionX(), visibleResolutionSize.height*0.9));
    
    ActionEase* ease;
    auto randomEase = random(0, 3);
    switch (randomEase) {
        case 0:
            ease = EaseBounceIn::create(move);
            break;
        case 1:
            ease = EaseElasticInOut::create(move);
            break;
        case 2:
            ease = EaseInOut::create(move, 0.5);
            break;
        case 3:
            ease = EaseBackOut::create(move);
            break;
        default:
            ease = EaseBounceInOut::create(move);
            break;
    }

 
    auto remove = RemoveSelf::create();
    
    this->runAction(Sequence::create(ease,remove, NULL));
    
    if (!GameData::GameOver) {
        cocos2d::log("releasing animal");
        this->addEvent();
    }
    
}

void Item::Death(const Vec2& position)
{
    if (this->getName() == "heart" || this->getName() == "timeflys") {
        this->escape(position);
        return;
    }
    if (!this->getReferenceCount()) {
        return;
    }
    _isDeath = true;
    
    this->retain();
    
    Node* layer;
    if (this->getParent() != NULL){
        //current Layer
        layer = this->getParent()->getParent();
        this->removeFromParent();
    }
    layer->addChild(this);
    this->setPosition(position);
    this->release();
    this->setOpacity(255);
    
    auto cache = SpriteFrameCache::getInstance();
    this->setSpriteFrame(cache->getSpriteFrameByName(this->getName()+"_death.png"));
    
    auto distance = this->getPosition().getDistance(Vec2(this->getPositionX(),visibleResolutionSize.height));
    auto time = distance / speed;
    
    auto move = MoveTo::create(time, Vec2(this->getPositionX(), 0));
    
    auto lambda = CallFunc::create([&](){
        auto HUD = (HUDLayer*)Director::getInstance()->getRunningScene()->getChildByName(__HUD_NAME__);
        
        if (HUD == NULL) {
            return;
        }
        for(auto charInfo : GameData::characterArr){
            if (this->getName() == charInfo->getName()) {
                HUD->updateHeart(-1);
                break;
            }
        }
        
    });
    
    auto remove = RemoveSelf::create();
    
    this->runAction(Sequence::create(move,lambda,remove, NULL));
    
    if (!GameData::GameOver) {
        cocos2d::log("releasing animal");
        this->addEvent();
    }
}
