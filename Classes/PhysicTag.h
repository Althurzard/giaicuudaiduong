//
//  PhysicTag.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/26/16.
//
//

#ifndef PhysicTag_h
#define PhysicTag_h

/*physic body tags*/
#define BUBBLENORMAL_PHYSIC_TAG 100
#define PLAYER_PHYSIC_BODY 101

/* specify bitmask for physic body*/
enum PhysicBitmask{
    Enemy = 0x04, //0100
    PlayerBullet = 0x05, // 0101
    
    Bubble = 0x02, //0010
    Bullet = 0x0A, //1010
    
    Wall = 0x01 //1100
    
};
#endif /* PhysicTag_h */
