//
//  BFAudioPlayer.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/6/17.
//
//

#ifndef BFAudioPlayer_h
#define BFAudioPlayer_h

#include <SimpleAudioEngine.h>
#include "GameData.h"

class BFAudioPlayer {
    
public:
    static void playBubbleExplode();
    static void playBackgroundMusic(const std::string& filename );
    static void pauseBackgroundMusic();
    static void resumeBackgroundMusic();
    static void stopBackgroundMusic();
};
#endif /* BFAudioPlayer_h */
