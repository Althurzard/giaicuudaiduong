//
//  StepFive.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/7/17.
//
//

#include "StepFive.h"


StepFive* StepFive::create(Node* node){
    auto ret = new StepFive;
    if (ret->init()) {
        ret->autorelease();
        ret->initOptions();
        node->addChild(ret,10);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
}

void StepFive::initOptions(){
    //bubble can drop down
    Director::getInstance()->getRunningScene()->getPhysicsWorld()->setSpeed(1);
    this->setTag(Step::Five);
    StepOne::setMesseages("Đây là Green Energy Bubble. Nó sẽ làm nhiễm độc và làm nổ tất cả các bóng rỗng hiện có.", Vec2(centerScene.x,centerScene.y - visibleResolutionSize.height*0.15));
    this->_step = Step::Six;
    StepOne::addButton();
    addEvent();
}

void StepFive::addEvent(){
    StepTwo::addEvent();
}


