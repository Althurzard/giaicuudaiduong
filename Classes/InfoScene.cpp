//
//  InfoScene.cpp
//  AngryRedHood
//
//  Created by Nguyen Quoc Vuong on 12/21/16.
//
//

#include "InfoScene.h"

#define _INFO_SCENE_NAME_ "INFOScene"
InfoScene* InfoScene::create(cocos2d::Scene* scene,const std::string& boardName){
    if (scene->getChildByName(_INFO_SCENE_NAME_)) {
        scene->getChildByName(_INFO_SCENE_NAME_)->removeFromParent();
    }
    
    auto ret = new InfoScene;
    
    if (ret->init()) {
        ret->autorelease();
        ret->initOptions(boardName);
        scene->addChild(ret,20);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
}

void InfoScene::initOptions(const std::string& boardName){
    
    this->setName(_INFO_SCENE_NAME_);
    //pause first scene
    auto menu = cocos2d::Director::getInstance()->getRunningScene()->getChildByName(__GAMEMENU_NAME__);
    GameHelper::pauseRecursive(menu, true);
    //bg
    auto bg = cocos2d::Sprite::create("mask.png");
    auto sX = visibleResolutionSize.width/bg->getContentSize().width;
    auto sY = visibleResolutionSize.height /bg->getContentSize().height;
    bg->setScale(sX, sY);
    bg->setPosition(centerScene);
    bg->setCascadeOpacityEnabled(true);
    bg->setOpacity(100);
    this->addChild(bg,1);
    
    //board
    _board = cocos2d::Sprite::create(boardName);
    sX = visibleResolutionSize.width * 0.8 / _board->getContentSize().width;
    sY = visibleResolutionSize.height * 0.25 / _board->getContentSize().height;
    _board->setScale(sX, sY);
    _board->setPosition(centerScene);
    this->addChild(_board,2);
    
    
}

void InfoScene::addEvent(){
    //exit btn
    _yesBtn = cocos2d::Sprite::create("yes_btn.png");
    _yesBtn->setScale(1.5, 1.3);
    _yesBtn->setPosition(visibleResolutionSize.width*0.5, _board->getBoundingBox().getMinY());
    this->addChild(_yesBtn,3);
    
    auto listener = cocos2d::EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = [&](cocos2d::Touch* touch, cocos2d::Event* event)
    {
        auto yesRect = this->_yesBtn->getBoundingBox();
        if (yesRect.containsPoint(touch->getLocation())) {
            
            //resume first scene
            auto scene = cocos2d::Director::getInstance()->getRunningScene()->getChildByName(__GAMEMENU_NAME__);
            GameHelper::pauseRecursive(scene, false);
            this->removeFromParent();
        }
        
        return false; // we did not consume this event, pass thru.
    };
    
    cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
    
    
}
