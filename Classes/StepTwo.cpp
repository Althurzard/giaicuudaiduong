//
//  StepTwo.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/7/17.
//
//

#include "StepTwo.h"


StepTwo* StepTwo::create(Node* node){
    auto ret = new StepTwo;
    if (ret->init()) {
        ret->autorelease();
        ret->initOptions();
        node->addChild(ret,10);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
}

void StepTwo::initOptions(){
    //bubble cant drop down
    Director::getInstance()->getRunningScene()->getPhysicsWorld()->setSpeed(0);
    this->setTag(Step::Two);
    auto bubble = BubbleNormal::create(this);
    bubble->setPosition(centerScene);

    StepOne::setMesseages("Chạm nhanh vào bong bóng!", Vec2(centerScene.x,
                                                          visibleResolutionSize.height*0.15));
    this->_step = Step::Two;
    this->scheduleUpdate();
}

void StepTwo::update(float dt){
    if (GameData::isTapObjects) {
        this->unscheduleUpdate();
        //go to next step
        BaseTutorialScene::nextStep(Step::Three);
        this->removeFromParent();
    } else if (this->getChildByName(BUBBLE_NORMAL_NAME) == nullptr) {
        //if bubble explode before player tap, we should notify and tap again
        this->unscheduleUpdate();
        addEvent();
        StepOne::setMesseages("Bạn phải chạm vào chúng trước khi chúng tự biến mất!", centerScene);
        StepOne::addButton();
        
    }
   
}

void StepTwo::addEvent(){
    StepOne::addEvent();
}
