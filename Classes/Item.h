//
//  Item.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/26/16.
//
//

#ifndef Item_h
#define Item_h
#include "cocos2d.h"

USING_NS_CC;

class Item: public cocos2d::Sprite {
private:
    Vector<SpriteFrame*>  _animation;
    int speed;
    bool _isDeath;
public:
    Item();
    ~Item();
    static Item* create(Node*,const std::string& name);
    
    void startAnimation();
    void initOptions(const std::string& name);
    void addEvent();
    void escape(const Vec2&);
    void get(const Vec2&);
    void Death(const Vec2& position);
};

#endif /* Item_h */
