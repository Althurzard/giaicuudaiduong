//
//  StepTwo.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/7/17.
//
//

#ifndef StepTwo_h
#define StepTwo_h
#include "StepOne.h"
#include "BubbleNormal.h"
#
class StepTwo: public StepOne {

public:
    StepTwo(){};
    ~StepTwo(){};
    
    static StepTwo* create(Node*);
    
    virtual void initOptions();
    virtual void addEvent();
    virtual void update(float dt);
};

#endif /* StepTwo_h */
