//
//  TimerLabel.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/3/17.
//
//

#include "TimerLabel.h"

TimerLabel::TimerLabel(){
    
}

TimerLabel::~TimerLabel(){
    
}

TimerLabel* TimerLabel::create(Node* node){
    auto ret = new TimerLabel;
    if(ret->initWithTTF("0", fileName("Chalkboard"), 30)){
        ret->autorelease();
        ret->initOptions();
        node->addChild(ret,2);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
}

void TimerLabel::initOptions(){
    this->setAlignment(TextHAlignment::CENTER);
    timeOver = false;
    
    //if play tutorial, we dont count time
    if (GameData::isPlayTutorial) {
        return;
    }
    switch (GameData::Mode) {
        case FREE:
        {
            GameData::Time = 0;
            auto time = 1.0f;
            auto wait = DelayTime::create(time);
            auto callback = CallFunc::create(CC_CALLBACK_0(TimerLabel::updateTime, this, time));
            auto sequence = Sequence::create(wait,callback, NULL);
            auto repeatForever = RepeatForever::create(sequence);
            this->runAction(repeatForever);
            break;
        }
            
            
        case CHALLENGE:
        {
            
            GameData::Time = GameData::levelDataArray.at(GameData::selectedLevel)->getTime();
            auto wait = DelayTime::create(1.0);
            auto callback = CallFunc::create(CC_CALLBACK_0(TimerLabel::updateTime, this,-1));
            auto sequence = Sequence::create(wait,callback, NULL);
            auto repeatForever = RepeatForever::create(sequence);
            this->runAction(repeatForever);
            break;
        }
    }
}

bool TimerLabel::isTimeOver(){
    return timeOver;
}

void TimerLabel::updateTime(float dt){
    GameData::Time += dt;
    this->setString(std::to_string(GameData::Time));
    if (GameData::Time == 0) {
        timeOver = true;
        this->stopUpdate();
    }
}

void TimerLabel::stopUpdate(){
    this->stopAllActions();
}
