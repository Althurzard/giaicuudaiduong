//
//  HighscoreBoard.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/30/16.
//
//

#ifndef HighscoreBoard_h
#define HighscoreBoard_h
#include "GameData.h"


class HighscoreBoard: public Node {
    
public:
    HighscoreBoard();
    ~HighscoreBoard();
    
    static HighscoreBoard* create(Node*);
    
    void initOptions();
};

#endif /* HighscoreBoard_h */
