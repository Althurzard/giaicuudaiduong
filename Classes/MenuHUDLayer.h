//
//  MenuHUDLayer.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/1/17.
//
//

#ifndef MenuHUDLayer_h
#define MenuHUDLayer_h
#include "GameData.h"
#include "BubbleButton.h"
#include "GameHelper.h"
class MenuHUDLayer: public Layer {
private:
    Sprite* _musicBtn;
public:
    MenuHUDLayer();
    ~MenuHUDLayer();
    
    static MenuHUDLayer* create(Scene*);
    
    void initOptions();
    void addEvent();
    void showBackBtn();
    
};

#endif /* MenuHUDLayer_h */
