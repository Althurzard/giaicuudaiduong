//
//  StepOne.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/7/17.
//
//

#ifndef StepOne_h
#define StepOne_h
#include "BaseGameScene.h"
#include "BubbleButton.h"
#include "BaseTutorialScene.h"



class StepOne: public Node {
protected:
    BubbleButton* _btnNext;
    BubbleButton* _btnSkip;
    Step _step;
public:
    StepOne();
    ~StepOne();
    
    static StepOne* create(Node*);
    
    virtual void setMesseages(const std::string& messeages,
                              const Vec2& position,
                              const bool& erasePreviousMess = true);
    virtual void initOptions();
    virtual void addButton();
    virtual void addEvent();
};

#endif /* StepOne_h */
