//
//  GameMenuScene.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/31/16.
//
//

#include "GameMenuScene.h"
#include "BFAudioPlayer.h"
#include "RandomFishNode.h"
bool GameMenuScene::firstTimeLogin = true;
GameMenuScene::GameMenuScene(){
    
}

GameMenuScene::~GameMenuScene(){
    
}

Scene* GameMenuScene::createScene(){
    auto scene = Scene::create();
    
    auto layer = GameMenuScene::create();
    
    scene->addChild(layer,1);
    
    
    return scene;
}

bool GameMenuScene::init(){
    
    
    if (!Layer::init()) {
        return false;
    }
    
    this->setName(__GAMEMENU_NAME__);
    
    if (GameMenuScene::firstTimeLogin) {
        GameMenuScene::firstTimeLogin = false;
        auto slashscreen = Sprite::create("slashscreen.png");
        auto sx = visibleResolutionSize.width / slashscreen->getContentSize().width;
        auto sy = visibleResolutionSize.height / slashscreen->getContentSize().height;
        slashscreen->setScale(sx, sy);
        slashscreen->setPosition(centerScene);
        this->addChild(slashscreen);
        
        auto delay = DelayTime::create(5.0);
        auto callback = CallFunc::create([&,slashscreen](){
            this->initOptions();
            slashscreen->removeFromParent();
        });
        auto sequence = Sequence::create(delay,callback, NULL);
        this->runAction(sequence);
    } else {
        initOptions();
    }
    
    
    
    return true;
    
}

void GameMenuScene::initOptions(){
    
    SonarCocosHelper::RevMob::preloadRewardAd();
    
    auto delay = DelayTime::create(1.0);
    auto callback = CallFunc::create([&](){
        MenuHUDLayer::create(Director::getInstance()->getRunningScene());
        SDKHelper::Admob::showBanner();
        SDKHelper::Admob::showFullscreen();
        
    });
    this->runAction(Sequence::create(delay,callback, NULL));
    
    //background
    auto bg = Sprite::create(fileName("MenuBackground"));
    auto sY = MAX(visibleResolutionSize.height / bg->getContentSize().height,
                  bg->getContentSize().height / visibleResolutionSize.height);
    
    auto sX = MAX(bg->getContentSize().width / visibleResolutionSize.width,
                  visibleResolutionSize.width / bg->getContentSize().width);
    bg->setScale(sX,sY);
    bg->setPosition(centerScene);
    this->addChild(bg);
    
    
    //animation;
    if (GameData::isPlayTutorial) {
        GameMenu::create(this);
    } else {
        GameMenuTutorial::create(this);
    }
    
    BFAudioPlayer::playBackgroundMusic("sound/tincture.mp3");
    
    auto touchListener = EventListenerKeyboard::create();
    touchListener->onKeyReleased = [](cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
    {
        if (keyCode == EventKeyboard::KeyCode::KEY_BACK) {
            Director::getInstance()->end();
        }
    };
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchListener, this);
    
    //random fish
    RandomFishNode::create(this);
}

void GameMenuScene::addEvent(){
    
}




