//
//  BubbleNormal.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/26/16.
//
//

#include "BubbleNormal.h"


BubbleNormal::BubbleNormal(){
    
}

BubbleNormal::~BubbleNormal(){
 
}

BubbleNormal* BubbleNormal::create(cocos2d::Layer* layer){
    auto ret = new BubbleNormal;
    
    if (ret->initWithFile(fileName("BubbleNormal"))) {
        ret->autorelease();
        ret->initOptions();
        ret->addEvent();
        layer->addChild(ret,5);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
}

BubbleNormal* BubbleNormal::create(cocos2d::Scene* scene){
    auto ret = new BubbleNormal;
    
    if (ret->initWithFile(fileName("BubbleNormal"))) {
        ret->autorelease();
        ret->initOptions();
        ret->addEvent();
        scene->addChild(ret,2);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
}

BubbleNormal* BubbleNormal::create(cocos2d::Node* node){
    auto ret = new BubbleNormal;
    
    if (ret->initWithFile(fileName("BubbleNormal"))) {
        ret->autorelease();
        ret->initOptions();
        ret->addEvent();
        node->addChild(ret,5);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
}

void BubbleNormal::initOptions(){
    this->setName(BUBBLE_NORMAL_NAME);
    auto randomX = cocos2d::random(0 + this->getBoundingBox().size.width / 2,
                                     visibleResolutionSize.width - this->getBoundingBox().size.width / 2);
    this->setPosition(randomX,0);
    this->setScale(0.7, 0.7);
    this->setTag(BUBBBLE_TAG);
    auto _physicBody = cocos2d::PhysicsBody::createCircle(this->getContentSize().width/2,PhysicsMaterial(0.5,0.2,0.2));
    _physicBody->setGravityEnable(true);
    _physicBody->setTag(BUBBLENORMAL_PHYSIC_TAG);
    _physicBody->setRotationEnable(false);
    _physicBody->setCategoryBitmask(PhysicBitmask::Bubble);
    _physicBody->setContactTestBitmask(PhysicBitmask::Bubble | PhysicBitmask::Bullet);
    this->setPhysicsBody(_physicBody);
    this->setCascadeOpacityEnabled(true);
    this->setCascadeColorEnabled(true);
    timer = 0;
    schedule(CC_SCHEDULE_SELECTOR(BubbleNormal::changeState), 1.0f);
    this->scheduleUpdate();
}

void BubbleNormal::update(float dt){
    if (GameData::GameOver) {
        this->Explode();
        return;
    }
}

void BubbleNormal::addEvent(){
    auto listener = cocos2d::EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = [&](cocos2d::Touch *touch, cocos2d::Event *event){
        //dont consume touch when game over
        if (GameData::GameOver) {
            return false;
        }
        auto rect = this->getBoundingBox();
        auto actulPos = this->getParent()->convertToNodeSpace(touch->getLocation());
        if (rect.containsPoint(actulPos)) {
            GameData::isTapObjects = true;
            ComboLabel::create(this->getParent(), actulPos);
            this->Explode();
            return true;
        } 
        return false;
    };
    
    
    listener->onTouchEnded = [&](cocos2d::Touch *touch, cocos2d::Event *event) {
        GameData::isTapObjects = false;
    };
    

    cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
}

void BubbleNormal::changeState(float dt){
   
    
    if (timer == 4) {
        this->setOpacity(100);
    }
    
    if (timer == 7) {
        this->removeFromParent();
    }
     timer ++;
}

void BubbleNormal::Explode(){
    BFAudioPlayer::playBubbleExplode();
    this->unscheduleUpdate();
    auto hudLayer = Director::getInstance()->getRunningScene()->getChildByName(__HUD_NAME__);
    ParticleHelper::initParticle(hudLayer,
                                 fileName("BubbleExplode"),
                                 this->getPosition(),
                                 2.5);
    this->removeFromParent();
}

int BubbleNormal::getTimer(){
    return timer;
}

