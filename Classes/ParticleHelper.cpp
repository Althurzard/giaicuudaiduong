//
//  ParticleHelper.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/26/16.
//
//

#include "ParticleHelper.h"

cocos2d::ParticleSystemQuad* ParticleHelper::initParticle(Node* node,
                                  const std::string& fileName,
                                  Vec2 pos,
                                  float scale){
    
    auto particleSystem = cocos2d::ParticleSystemQuad::create(fileName);
    particleSystem->setName(PARTICLE_NAME);
    particleSystem->setPositionType(cocos2d::ParticleSystem::PositionType::RELATIVE);
    particleSystem->setPosition(pos);
    particleSystem->setScale(scale);
    particleSystem->setAutoRemoveOnFinish(true);
    node->addChild(particleSystem,10);
    return particleSystem;
}

