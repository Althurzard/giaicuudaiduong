//
//  ParticleHelper.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/26/16.
//
//

#ifndef ParticleHelper_h
#define ParticleHelper_h
#include "cocos2d.h"
#define PARTICLE_NAME "PARTICLE NAME"
USING_NS_CC;

class ParticleHelper {
    
public:
    static cocos2d::ParticleSystemQuad* initParticle(Node* ,const std::string& fileName,Vec2,float scale = 0.3);
};
#endif /* ParticleHelper_h */
