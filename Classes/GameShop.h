//
//  GameShop.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/1/17.
//
//

#ifndef GameShop_h
#define GameShop_h
#include "BaseGameMenu.h"
#include "SDKHelper.h"
#include "InfoScene.h"
#define GAME_SHOP "Gameshop"
class GameShop: public BaseGameMenu {
private:
    
    
    Sprite* _bubbleSelect;
public:
    GameShop();
    ~GameShop();
    
    static GameShop* create(Node*);
    std::string characterSelect;
    
    void initOptions();
    void createBubble();
    void addEvent();
    void hide();
    void unlockCharacter(Item*);
    void boughtBubble(const std::string& name);
};
#endif /* GameShop_h */
