//
//  GreenBubble.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/29/16.
//
//

#ifndef GreenBubble_h
#define GreenBubble_h
#include "BubbleNormal.h"
#define SPECIAL_GREEN_BUBBLE_NAME "Special green bubble"
class GreenBubble: public BubbleNormal {
    
public:
    GreenBubble();
    ~GreenBubble();
    
    static GreenBubble* create(Layer*);
    static GreenBubble* create(Node*);
    virtual void initOptions();
    virtual void addEvent();
    virtual void Explode();
    virtual void changeState(float dt);
};

#endif /* GreenBubble_h */
