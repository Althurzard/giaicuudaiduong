//
//  StepOne.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/7/17.
//
//

#include "StepOne.h"

StepOne::StepOne(){
    
}

StepOne::~StepOne(){
    
}

StepOne* StepOne::create(Node* node ){
    auto ret = new StepOne;
    if (ret->init()) {
        ret->autorelease();
        ret->initOptions();
        ret->addEvent();
        node->addChild(ret,10);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
}

void StepOne::initOptions(){
    this->setTag(Step::One);
    addButton();
    _step = Step::Two;
    this->setMesseages("Bạn có muốn hướng dẫn chơi?", centerScene);
    
    
}

void StepOne::setMesseages(const std::string& messeages,
                           const Vec2& position,
                           const bool& ereasePreviousMess){
    if (ereasePreviousMess) {
        for (auto child : this->getChildren()) {
            if (child->getName() == "Messeages" ) {
                child->removeFromParent();
            }
        }
        
    }
    
    auto lblMess = Label::createWithTTF(messeages, fileName("MarkerFelt"), 35);
    lblMess->setName("Messeages");
    lblMess->setPosition(position);
    lblMess->setAlignment(TextHAlignment::CENTER);
    lblMess->setDimensions(visibleResolutionSize.width * 0.8, 0);
    this->addChild(lblMess,10);
}

void StepOne::addButton(){
    
    auto padding = 10;
    _btnNext = BubbleButton::create(this, 1.2, fileName("Next"), 0.3);
    _btnNext->setPosition(centerScene.x + _btnNext->getBoundingBox().size.width/2 + padding,
                          visibleResolutionSize.height * 0.1);
    
    _btnSkip = BubbleButton::create(this, 1.2, fileName("Skip"), 0.3);
    _btnSkip->setPosition(centerScene.x - _btnSkip->getBoundingBox().size.width/2 - padding ,
                          visibleResolutionSize.height * 0.1);
}

void StepOne::addEvent(){
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = [&](Touch* touch, Event* event){
        auto realPos = this->convertTouchToNodeSpace(touch);
        auto nextRect = this->_btnNext->getBoundingBox();
        auto skipRect = this->_btnSkip->getBoundingBox();
        
        if (nextRect.containsPoint(realPos)) {
            this->_btnNext->Explode();
            BaseTutorialScene::nextStep(this->_step);
            this->removeFromParent();
            return true;
        }
        
        if (skipRect.containsPoint(realPos)) {
            GameData::isPlayTutorial = false;
            GameData::saveData();
            this->_btnSkip->Explode();
            auto scene = BaseGameScene::createScene();
            Director::getInstance()->replaceScene(TransitionFade::create(0.5, scene));
            return true;
        }
        
        return false;
    };
    
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
}
