//
//  BaseGameScene.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/26/16.
//
//

#ifndef BaseGameScene_h
#define BaseGameScene_h
#include "BubbleFrog.h"
#include "GreenBubble.h"
#include "BlueBubble.h"
#include "GameHelper.h"
#include "MainGameBackground.h"
#include "RandomFishNode.h"
USING_NS_CC;
class BaseGameScene: public Layer {
private:
    float freeModeTime;
    
public:
    BaseGameScene();
    ~BaseGameScene();
    
    PhysicsWorld* m_world;
    static Scene* createScene();
    
    virtual bool init();
    virtual void initOptions();
    virtual void setPhysicWorld(PhysicsWorld* world);
    virtual void scheduleToRespawn(const float& time);
    virtual void respawnBubble();
    virtual void eliminateAllNormalBubble();
    virtual void eliminateAllBubble();
    virtual void stopGame();
    virtual void startShooting(const Vec2& postion);
    virtual void initBullet(const Vec2& postion);
    //override touch events, update and physics contact
    virtual bool onContactBegin(PhysicsContact& contact);
    virtual void update(float dt);
    virtual void onTouchesEnded(const std::vector<cocos2d::Touch *> &touch, cocos2d::Event *event);
    CREATE_FUNC(BaseGameScene);
};

#endif /* BaseGameScene_h */
