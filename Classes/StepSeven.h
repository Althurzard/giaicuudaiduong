//
//  StepSeven.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/7/17.
//
//

#ifndef StepSeven_h
#define StepSeven_h
#include "StepTwo.h"

class StepSeven: public StepTwo {
    
public:
    StepSeven(){};
    ~StepSeven(){};
    
    static StepSeven* create(Node*);
    
    virtual void initOptions();
    virtual void addEvent();
    
};
#endif /* StepSeven_h */
