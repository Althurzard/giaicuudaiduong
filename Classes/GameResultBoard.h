//
//  GameResultBoard.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/30/16.
//
//

#ifndef GameResultBoard_h
#define GameResultBoard_h
#include "GameData.h"
#include "Item.h"

class GameResultBoard: public Node {
    
public:
    GameResultBoard();
    ~GameResultBoard();
    
    static GameResultBoard* create(Node*,const GameResult&);
    
    void initOptions(const GameResult&);
    
    Sprite* createBubble(const Vec2&);
};

#endif /* GameResultBoard_h */
