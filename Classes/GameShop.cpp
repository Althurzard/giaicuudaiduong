//
//  GameShop.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/1/17.
//
//

#include "GameShop.h"
#include "GameMenu.h"
#include "SDKHelper.h"
#define WaitTime 0.3f

GameShop::GameShop(){
  
}

GameShop::~GameShop(){

}

GameShop* GameShop::create(Node* layer){
    
    auto ret = new GameShop;
    if (ret->init()) {
        ret->autorelease();
        ret->initOptions();
        layer->addChild(ret,10);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
}

void GameShop::initOptions(){
    
    
    this->setName(GAME_SHOP);
    createBubble();
    
    BaseGameMenu::initOptions();
    
    //animation
    
    auto sequence = GameHelper::squenceScaleInOut(btnArray, 0.3f);
    this->runAction(sequence);
    this->addEvent();
    
}

void GameShop::createBubble(){
    for (auto charInfo : GameData::characterArr) {
        if (charInfo->isSelected()) {
            _bubbleSelect = Sprite::create(fileName("BubbleNormal"));
            _bubbleSelect->setScale(0.7);
            _bubbleSelect->setPosition(charInfo->getPosition());
            _bubbleSelect->setCascadeOpacityEnabled(true);
            _bubbleSelect->setOpacity(0);
            this->addChild(_bubbleSelect);
            
            auto select = Sprite::create(fileName("Select"));
            select->setScale(0.3);
            select->setPosition(_bubbleSelect->getBoundingBox().size.width * 0.72,
                                _bubbleSelect->getBoundingBox().size.height * 1.7);
            _bubbleSelect->addChild(select);
            
            
            auto item = Item::create(this, charInfo->getName());
            item->setScale(1.8);
            item->setPosition(charInfo->getPosition());
            item->setOpacity(0);
            
            btnArray.pushBack(_bubbleSelect);
            btnArray.pushBack(item);
            
        } else if (charInfo->isUnlock()) {
            
            auto item = Item::create(this, charInfo->getName());
            item->setScale(1.8);
            item->setPosition(charInfo->getPosition());
            item->setOpacity(0);
            btnArray.pushBack(item);
        
        } else {
            auto bubble = Sprite::create(fileName("BubbleButton"));
            bubble->setName("BubbleButton");
            bubble->setScale(1.4);
            bubble->setCascadeOpacityEnabled(true);
            bubble->setPosition(charInfo->getPosition());
            this->addChild(bubble);
            auto item = Item::create(bubble, charInfo->getName());
            item->setScale(1.3);
            btnArray.pushBack(bubble);
            bubble->setOpacity(0);
            
        }
        
    }
    

}


void GameShop::addEvent(){
    auto listener = cocos2d::EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = [&](cocos2d::Touch *touch, cocos2d::Event *event){
        
        auto realPos = this->convertTouchToNodeSpace(touch);
        auto backRect = this->_backBtn->getBoundingBox();
       
        if (backRect.containsPoint(realPos)) {
            this->_backBtn->Explode();
            this->btnArray.eraseObject(this->_backBtn);
            this->hide();
            BaseGameMenu::goToPage(this, Page::Menu);
            return true;
        } else {
            for (auto btn : this->btnArray) {
                auto btnRect = btn->getBoundingBox();
                
                if (btnRect.containsPoint(realPos)) {
                    
                    //select character is unlocked and no selected yet.
                    if (typeid(*btn) == typeid(Item)){
                        auto charInfo = GameData::getCharacterData(btn->getName());
                        //not selected yet.
                        if (charInfo != nullptr && !charInfo->isSelected()) {
                            // bubble-select animation
                            auto moveTo = MoveTo::create(0.2, btn->getPosition());
                            this->_bubbleSelect->runAction(moveTo);
                            
                            //save new state of last selected character
                            // make it UNselected
                            auto oldCharacter = GameData::getCharacterData(GameData::characterName);
                            oldCharacter->setSelect(false);
                            
                            //save new state of new character is selected
                            GameData::characterName = charInfo->getName();
                            charInfo->setSelect(true);
    
                        }
                        return true;
                    }
                    
                    if (btn->getName() == "BubbleButton") {
                        
                        BFAudioPlayer::playBubbleExplode();
                        
                        
                        for (auto child : btn->getChildren()) {
                            if (typeid(*child) == typeid(Item)) {
                                
                                if (SDKHelper::RevMob::isCacheRewardVideos) {
                                    SDKHelper::RevMob::isCacheRewardVideos = false;
                                    characterSelect = child->getName();
                                    SonarCocosHelper::RevMob::showRewardAd();
                                    
                                } else {
                                    SonarCocosHelper::RevMob::preloadRewardAd();
                                    auto scene = Director::getInstance()->getRunningScene();
                                    auto info = InfoScene::create(scene, fileName("NotAvailable"));
                                    info->addEvent();
                                }
                                return true;
                            }
                        }
                        
                        
                    }
                }
            }
        }


        
        return false;
    };
    
    
    listener->onTouchEnded = [&](cocos2d::Touch *touch, cocos2d::Event *event) {
        
    };
    
    
    cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
}

void GameShop::hide(){
    
    BaseGameMenu::hide();
    
}

void GameShop::unlockCharacter(Item* child){
    //unlock
    //character in the bubble
    //release child and add to scene
    auto tempPositon = child->getParent()->getPosition();
    child->retain();
    child->removeFromParent();
    child->setScale(1.8);
    child->setPosition(tempPositon);
    this->addChild(child);
    child->startAnimation();
    child->release();
    
    //update state when unlock new character
    auto charInfo = GameData::getCharacterData(child->getName());
    if (charInfo != nullptr) {
        charInfo->setUnlock(true);
    }
    
    ParticleHelper::initParticle(this,
                                 fileName("BubbleButtonExplode"),
                                 tempPositon,
                                 1.0);
    btnArray.pushBack(child);
}


void GameShop::boughtBubble(const std::string& name){
    for (auto btn : this->btnArray) {
        if (btn->getName() == "BubbleButton"){
            for (auto child : btn->getChildren()) {
                if (typeid(*child) == typeid(Item) && child->getName() == name) {
                    unlockCharacter((Item*)child);
                    btn->removeFromParent();
                    btnArray.eraseObject(btn);
                    auto delay = DelayTime::create(0.5);
                    auto callback = CallFunc::create([&,name](){
                        auto scene = Director::getInstance()->getRunningScene();
                        std::string filePath = "got" + name + ".png";
                        auto info = InfoScene::create(scene, filePath);
                        info->addEvent();
                    });
                    this->runAction(Sequence::create(delay,callback, NULL));
                    SonarCocosHelper::RevMob::preloadRewardAd();
                    return;
                }
            }
        }
    }
}

