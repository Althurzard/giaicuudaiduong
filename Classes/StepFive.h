//
//  StepFive.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/7/17.
//
//

#ifndef StepFive_h
#define StepFive_h
#include "StepTwo.h"

class StepFive: public StepTwo {
    
public:
    StepFive(){};
    ~StepFive(){};
    
    static StepFive* create(Node*);
    
    virtual void initOptions();
    virtual void addEvent();
    
    
};
#endif /* StepFive_h */
