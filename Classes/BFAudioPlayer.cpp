//
//  BFAudioPlayer.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/6/17.
//
//

#include "BFAudioPlayer.h"


static bool isPlaying = false;

void BFAudioPlayer::playBubbleExplode(){
    if (GameData::isTurnOffMusic) {
        return;
    }
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("sound/piggy_plop.wav");
}

void BFAudioPlayer::playBackgroundMusic(const std::string& fileName){
    if (GameData::isTurnOffMusic) {
        return;
    }
    isPlaying = true;
    if (CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying()) {
        CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
    }

    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic(fileName.c_str(),true);
}

void BFAudioPlayer::pauseBackgroundMusic(){
    if (isPlaying) {
        CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
    }
}

void BFAudioPlayer::resumeBackgroundMusic(){
    if (isPlaying) {
        CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
    }
}

void BFAudioPlayer::stopBackgroundMusic(){
    CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
}
