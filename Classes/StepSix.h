//
//  StepSix.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/7/17.
//
//

#ifndef StepSix_h
#define StepSix_h
#include "StepTwo.h"

class StepSix: public StepTwo {
    
public:
    StepSix(){};
    ~StepSix(){};
    
    static StepSix* create(Node*);
    
    virtual void initOptions();
    virtual void addEvent();
};

#endif /* StepSix_h */
