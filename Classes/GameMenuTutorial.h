//
//  GameMenuTutorial.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/7/17.
//
//

#ifndef GameMenuTutorial_h
#define GameMenuTutorial_h
#include "GameMenu.h"

class GameMenuTutorial: public GameMenu {
private:
    BubbleButton* _btnTutorial;
public:
    GameMenuTutorial(){};
    ~GameMenuTutorial(){};
    
    static GameMenuTutorial* create(Node*);
    
    virtual void initOptions();
    virtual void addEvent();
    virtual void hide();
};

#endif /* GameMenuTutorial_h */
