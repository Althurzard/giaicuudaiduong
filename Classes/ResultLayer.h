//
//  ResultBoard.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/30/16.
//
//

#ifndef ResultLayer_h
#define ResultLayer_h
#include "BubbleButton.h"
#include "HighscoreBoard.h"
#include "GameResultBoard.h"
#include "GameHelper.h"

class ResultLayer: public Layer {
private:
    BubbleButton* _rateBtn;
    BubbleButton* _nextBtn;
    BubbleButton* _homeBtn;
    
    Vector<Node*> btnArray;
    HighscoreBoard* _highScoreBoard;
    GameResultBoard* _gameResultBoard;
    
    GameResult result;
public:
    ResultLayer();
    ~ResultLayer();
    
    static ResultLayer* create(Scene*,const GameResult&);
    
    void initOptions(const GameResult&);
    void addEvent();
    void switchScene();
};

#endif /* ResultLayer.h */
