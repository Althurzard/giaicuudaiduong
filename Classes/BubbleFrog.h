//
//  BubbleFrog.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/26/16.
//
//

#ifndef BubbleFrog_h
#define BubbleFrog_h
#include "BubbleNormal.h"
#include "Item.h"
#define BUBBLE_ITEM_NAME "BUBBLE ITEM NAME"
class BubbleFrog: public BubbleNormal{
private:
    Item* _item;
    
public:
    BubbleFrog();
    ~BubbleFrog();
    

    static BubbleFrog* create(cocos2d::Layer*, const std::string& itemName);
    static BubbleFrog* create(Node* layer,const std::string& itemName);
    virtual void initOptions();
    virtual void addEvent();
    virtual void Explode();
    virtual void initItem(const std::string& itemName);
    virtual void changeState(float dt);
};

#endif /* BubbleFrog_h */
