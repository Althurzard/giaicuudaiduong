//
//  MissLabel.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/27/16.
//
//

#ifndef MissLabel_h
#define MissLabel_h
#include "GameData.h"

class MissLabel: public Label {
    
public:
    MissLabel();
    ~MissLabel();
    
    static MissLabel* create(Node*,const Vec2&);
    
    void initOptions(const Vec2&);
};

#endif /* MissLabel_h */
