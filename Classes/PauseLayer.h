//
//  PauseLayer.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/6/17.
//
//

#ifndef PauseLayer_h
#define PauseLayer_h
#include "GameHelper.h"

class PauseLayer: public Layer {
private:
    Sprite* _resumeBtn;
public:
    PauseLayer();
    ~PauseLayer();
    
    static PauseLayer* create(Scene*);
    void initOption();
    void addEvent();
    static void pauseGame(const bool&);
};

#endif /* PauseLayer_h */
