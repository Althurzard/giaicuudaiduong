//
//  StepSix.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/7/17.
//
//

#include "StepSix.h"


StepSix* StepSix::create(Node* node){
    auto ret = new StepSix;
    if (ret->init()) {
        ret->autorelease();
        ret->initOptions();
        node->addChild(ret,10);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
}

void StepSix::initOptions(){
    //bubble can drop down
    this->setTag(Step::Six);
    StepOne::setMesseages("Đây là Blue Energy Bubble. Nó sẽ tạo ra những viên đạn nước bắn xung quanh chúng", Vec2(centerScene.x,centerScene.y - visibleResolutionSize.height*0.15));
    this->_step = Step::Seven;
    StepOne::addButton();
    addEvent();
}

void StepSix::addEvent(){
    StepTwo::addEvent();
}
