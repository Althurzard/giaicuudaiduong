//
//  SDKHelper.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/8/17.
//
//

#ifndef SDKHelper_h
#define SDKHelper_h


#include "PluginOneSignal/PluginOneSignal.h"
namespace SDKHelper {
    
    
    class Admob {
        
    public:
        static void init();
        static void showFullscreen();
        static void showBanner();
        static void hideBanner();
    };
    
    class IAP {
    public:
        static void init();
        static void purchase(const std::string&);
        static void restore();
        static void refresh();
    };
    

    class RevMob {
    public:
        static bool isCacheRewardVideos;
        static bool isCompleteViewReward;
    };

}

#endif /* SDKHelper_h */
