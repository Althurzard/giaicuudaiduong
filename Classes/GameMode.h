//
//  GameMode.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/3/17.
//
//

#ifndef GameMode_h
#define GameMode_h
#include "BaseGameMenu.h"

class GameMode: public BaseGameMenu {
private:
    BubbleButton* _freeBtn;
    BubbleButton* _challengeBtn;
public:
    GameMode();
    ~GameMode();
    
    static GameMode* create(Node*);
    
    void initOptions();
    void addEvent();
    void hide();
};

#endif /* GameMode_h */
