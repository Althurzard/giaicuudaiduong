//
//  BaseGameScene.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/26/16.
//
//

#include "BaseGameScene.h"
#include "HUDLayer.h"
#include "BFAudioPlayer.h"
#include "SDKHelper.h"
#define BULLET_TAG 20
#define RESPAWN_TAG 21


BaseGameScene::BaseGameScene(){
    freeModeTime = 1.0;
}

BaseGameScene::~BaseGameScene(){
    this->removeAllChildren();
}

Scene* BaseGameScene::createScene(){
    auto scene = Scene::createWithPhysics();
    
    auto layer = BaseGameScene::create();
    layer->setPhysicWorld(scene->getPhysicsWorld());
    scene->getPhysicsWorld()->setGravity(Vec2(0,100));
    auto hud = HUDLayer::createScene(scene);
    hud->setCurrentGameScene(layer);
    //RandomFishNode::create(scene);
    scene->addChild(layer,2);
    

    return scene;
    
}


bool BaseGameScene::init(){
    
    if (!Layer::init()) {
        return false;
    }
    
    this->setName(__GAME_SCENE__);
    
    auto listener = EventListenerTouchAllAtOnce::create();
    listener->onTouchesEnded = CC_CALLBACK_2(BaseGameScene::onTouchesEnded, this);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
    
    auto contactListener = EventListenerPhysicsContact::create();
    contactListener->onContactBegin = CC_CALLBACK_1(BaseGameScene::onContactBegin, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(contactListener, this);

    auto ac = EventListenerTouchAllAtOnce::create();
    ac->onTouchesEnded = CC_CALLBACK_2(BaseGameScene::onTouchesEnded, this);
    
    //edge of the box
    auto edgeSp = Sprite::create();
    auto boundBody = PhysicsBody::createEdgeBox(visibleResolutionSize*1.1, PHYSICSBODY_MATERIAL_DEFAULT,1);
    edgeSp->setPosition(centerScene);
    edgeSp->setPhysicsBody(boundBody);
    this->addChild(edgeSp);
    edgeSp->setTag(PhysicBitmask::Wall);
    
    auto edgeTop = Sprite::create();
    auto abc = Size(visibleResolutionSize.width, visibleResolutionSize.height*0.15);
    auto boundBodyTop = PhysicsBody::createBox(abc,PHYSICSBODY_MATERIAL_DEFAULT);
    boundBodyTop->setGravityEnable(false);
    boundBodyTop->setDynamic(false);
    edgeTop->setPosition(Vec2(centerScene.x,visibleResolutionSize.height*0.87));
    edgeTop->setPhysicsBody(boundBodyTop);
    this->addChild(edgeTop);
    edgeTop->setTag(PhysicBitmask::Wall);
    
    //background
    MainGameBackground::create(this);
    
    initOptions();

    BFAudioPlayer::playBackgroundMusic("sound/background.mp3");
    
    SDKHelper::Admob::hideBanner();
    
    auto touchListener = EventListenerKeyboard::create();
    touchListener->onKeyReleased = [](cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
    {
        if (keyCode == EventKeyboard::KeyCode::KEY_BACK) {
            Director::getInstance()->end();
        }
    };
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchListener, this);
    return true;
}

void BaseGameScene::setPhysicWorld(PhysicsWorld* world){
    m_world = world;
    
}

void BaseGameScene::respawnBubble(){
    auto randomBubble = cocos2d::random(0, 20);
    if (randomBubble % 15 == 0) {
        
        auto random  = cocos2d::random(0, 3);
        
        switch (random) {
            case 0:
                BubbleFrog::create(this, "heart");
                break;
            case 1:
                if (GameData::Mode == MODE::CHALLENGE) {
                    BubbleFrog::create(this, "timeflys");
                }
                break;
            case 2:
                BlueBubble::create(this);
                break;
            case 3:
                GreenBubble::create(this);
                break;
            default:
                break;
        }
        
    } else  if (randomBubble % 2 == 0){

        BubbleFrog::create(this, GameData::characterName);
    } else {
        
        BubbleNormal::create(this);
    }
    
}

void BaseGameScene::initOptions(){
    
    switch (GameData::Mode) {
        case FREE:
        {
            this->scheduleToRespawn(freeModeTime);
            if (this->freeModeTime >= 0.3) {
                this->freeModeTime -= 0.1;
                //recursive every 30s, respawn time of bubbles will be more faster
                auto callback = CallFunc::create([&](){
                    this->initOptions();
                    cocos2d::log("Next wave is started at %f",this->freeModeTime);
                });
                auto delayWave1 = DelayTime::create(30.0);
                auto sequence = Sequence::create(delayWave1,callback, NULL);
                this->runAction(sequence);
            }
            
        }
            break;
        case CHALLENGE:
            auto delay = GameData::levelDataArray.at(GameData::selectedLevel)->getRespawnTime();
            scheduleToRespawn(delay);
    }
    
}

void BaseGameScene::scheduleToRespawn(const float& time){
   
    if (getActionByTag(RESPAWN_TAG)) {
        stopActionByTag(RESPAWN_TAG);
    }
    auto delay = DelayTime::create(time);
    auto callback = CallFunc::create(CC_CALLBACK_0(BaseGameScene::respawnBubble, this));
    auto sequence = Sequence::create(callback,delay, NULL);
    auto repeat = RepeatForever::create(sequence);
    repeat->setTag(RESPAWN_TAG);
    this->runAction(repeat);
    
}

void BaseGameScene::onTouchesEnded(const std::vector<Touch*> &touch, Event* event){
    GameData::isTapObjects = false;
}


bool BaseGameScene::onContactBegin(PhysicsContact &contact){
    
    auto nodeA = contact.getShapeA()->getBody()->getNode();
    auto nodeB = contact.getShapeB()->getBody()->getNode();
    
    if (nodeA && nodeB)
    {
        //bullet collide bubble
        if (nodeA->getTag() == BUBBBLE_TAG && nodeB->getTag() == BULLET_TAG)
        {
            ComboLabel::create(this, nodeA->getPosition());
            ((BubbleNormal*)nodeA)->Explode();
        } else if (nodeA->getTag() == BULLET_TAG && nodeB->getTag() == BUBBBLE_TAG) {
            ComboLabel::create(this, nodeB->getPosition());
            ((BubbleNormal*)nodeB)->Explode();
        }
    }
    
    //bodies can collide
    return true;
}

void BaseGameScene::stopGame(){
    this->unscheduleUpdate();
    this->stopAllActions();
}

void BaseGameScene::update(float dt){
    
}

void BaseGameScene::eliminateAllBubble(){
    for (auto bubble : this->getChildren()) {
        if (bubble->getName() == BUBBLE_NORMAL_NAME) {
            ((BubbleNormal*)bubble)->Explode();
        }
    }
}

void BaseGameScene::eliminateAllNormalBubble(){
    for (auto bubble : this->getChildren()) {
        if (typeid(*bubble) == typeid(BubbleNormal)) {
            
            bubble->unscheduleUpdate();
            auto colozie  = TintTo::create(0.8, 0, 216, 64); // green color
            auto callback = CallFunc::create([&,bubble](){
                ParticleHelper::initParticle(this,
                                             fileName("GreenBubbleExplode"),
                                             bubble->getPosition(),
                                             2.5);
                ComboLabel::create(this, bubble->getPosition());
                BFAudioPlayer::playBubbleExplode();
            });
            auto removeself = RemoveSelf::create();
            bubble->runAction(Sequence::create(colozie,callback,removeself, NULL));
        }
    }
}

void BaseGameScene::startShooting(const Vec2& position){
    
    //init new effect
    auto particle = ParticleHelper::initParticle(this,
                                                 fileName("SpecialBlueBubble"),
                                                 position,
                                                 1.0);
    auto wait = DelayTime::create(5.0);
    auto removesafe = RemoveSelf::create();
    particle->runAction(Sequence::create(wait,removesafe, NULL));
    
    
    auto waitForCallback = DelayTime::create(0.3);
    auto callback = CallFunc::create([&,position](){
        this->initBullet(position);
    });
    auto sequence = Sequence::create(waitForCallback,callback, NULL);
    auto repeat = Repeat::create(sequence, 16);
    this->runAction(repeat);
}

void BaseGameScene::initBullet(const Vec2& position){
    

    auto bullet = Sprite::create(fileName("Bullet"));
    bullet->setScale(0.6,0.6);
    bullet->setPosition(position);
    bullet->setTag(BULLET_TAG);
    
    auto randomX = random(0.0f, visibleResolutionSize.width);
    auto randomY = random(0.0f, visibleResolutionSize.height);
    auto randomPos = Vec2(randomX,randomY);
    
    auto desiredPosition = position.getMidpoint(randomPos);
    
    auto desiredRotation = GameHelper::getDesiredRotationTowards(position,
                                                                 desiredPosition);
    bullet->setRotation(desiredRotation);
    this->addChild(bullet,3);
    
    auto _physicBody = cocos2d::PhysicsBody::createBox(bullet->getContentSize());
    _physicBody->setGravityEnable(false);
    _physicBody->setDynamic(false);
    _physicBody->setCategoryBitmask(PhysicBitmask::Bullet);
    _physicBody->setContactTestBitmask(PhysicBitmask::Bubble);
    bullet->setPhysicsBody(_physicBody);
    bullet->getPhysicsBody()->setRotationOffset(0);
    
    auto time = desiredPosition.getDistance(position) / 300;
    
    auto move = MoveTo::create(time, desiredPosition);
    auto removeself = RemoveSelf::create();
    bullet->runAction(Sequence::create(move,removeself, NULL));
    
    
}
