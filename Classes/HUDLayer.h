//
//  HUDLayer.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/27/16.
//
//

#ifndef HUDLayer_h
#define HUDLayer_h
#include "GameHelper.h"
#include "ResultLayer.h"
#include "TimerLabel.h"
#include "PauseLayer.h"
class HUDLayer: cocos2d::Layer {
private:
    BubbleButton* _bubbleTimer;
    Vector<Sprite*> _heart;
    Label* _score;
    Label* _mCombo;
    TimerLabel* _timer;
    Label* _countFrogs;
    Label* _countDeathFrogs;
    LevelData* currentLevel;
    Sprite* _pauseBtn;
    Node* _gameScene;
    
    EventListenerTouchOneByOne* listener;
public:
    HUDLayer();
    ~HUDLayer();
    
    static HUDLayer* createScene(Scene*);
    
    void setCurrentGameScene(Node*);
    void initOptions();
    void addEvent();
    void update(float dt);
    void updateHeart(int);
    void updateScore();
    void updateCountFrogs();
    void updateCountDeathFrogs();
    void updateMCombo();
    void startTimingCombo();
    void endGame(const GameResult&);
};

#endif /* HUDLayer_h */
