//
//  GameMenu.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/31/16.
//
//

#ifndef GameMenu_h
#define GameMenu_h
#include "BaseGameMenu.h"
class GameMenu: public BaseGameMenu {
protected:
    Sprite* bubbleLogo;
    Sprite* frogLogo;
    BubbleButton* _startBtn;
    BubbleButton* _shopBtn;
    BubbleButton* _AboutBtn;
    BubbleButton* _moreGameBtn;
    
    
public:
    GameMenu();
    ~GameMenu();
    
    static GameMenu* create(Node*);
    
    virtual void initOptions();
    virtual void addEvent();
    virtual void hide();
};

#endif /* GameMenu_h */
