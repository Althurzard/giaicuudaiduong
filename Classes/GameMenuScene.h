//
//  GameMenuScene.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/31/16.
//
//

#ifndef GameMenuScene_h
#define GameMenuScene_h
#include "MenuHUDLayer.h"
#include "GameMenuTutorial.h"
#include "SDKHelper.h"
class GameMenuScene: public Layer {
private:
    
public:
    GameMenuScene();
    ~GameMenuScene();
    
    static Scene* createScene();
    
    virtual bool init();
    void initOptions();
    void addEvent();
    static bool firstTimeLogin;
    
    CREATE_FUNC(GameMenuScene);
};

#endif /* GameMenuScene_h */
