//
//  GameData.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/26/16.
//
//

#include "GameData.h"
#define _BEST_SCORE_ "BestScore"
#define _BEST_CAUGHT_ "BestCaught"
#define _BEST_COMBO_ "BestCombo"
#define _MAX_FISHDEATH_ "FishDeath"
#define _TURN_OFF_MUSIC_ "TURNOFFMUSIC"
#define _TUTORIAL_ "TUTORIAL"

Document GameData::filePath = Document();
std::string GameData::characterName = "";
std::vector<Character*> GameData::characterArr = {};
std::vector<LevelData*> GameData::levelDataArray = {};
MODE GameData::Mode = MODE::FREE; //default is free mode

int GameData::Time = 0;
int GameData::Life = MAX_LIFE;
int GameData::Score = 0;
int GameData::bestScore = 0;

int GameData::caught = 0;
int GameData::bestCaught = 0;

int GameData::fishDeath = 0;
int GameData::maxFishDeath = 0;

int GameData::tempCombo = 0;
int GameData::currentCombo = 0;
int GameData::bestCombo = 0;

bool GameData::isTapObjects = false;
bool GameData::GameOver = false;
bool GameData::isTurnOffMusic = false;
bool GameData::isPlayTutorial = true;

int GameData::selectedLevel = 0;

//position when catching item
Vec2 GameData::HeartPosition = Vec2();
Vec2 GameData::FrogPosition = Vec2();
Vec2 GameData::TimerPosition = Vec2();
Vec2 GameData::DeathFrogPosition = Vec2();

void GameData::loadData(){
    //Read json file path of all sprite
    std::string fullPath = "game_data.json";
    auto fileData = FileUtils::getInstance()->getDataFromFile(fullPath);
    std::string content((const char*)fileData.getBytes(),fileData.getSize());
    filePath.Parse(content.c_str());
    

    //music
    GameData::isTurnOffMusic = UserDefault::getInstance()->getBoolForKey(_TURN_OFF_MUSIC_);
    
    //load best score
    GameData::bestScore = UserDefault::getInstance()->getIntegerForKey(_BEST_SCORE_,0);
    GameData::bestCombo = UserDefault::getInstance()->getIntegerForKey(_BEST_COMBO_,0);
    GameData::bestCaught = UserDefault::getInstance()->getIntegerForKey(_BEST_CAUGHT_,0);
    GameData::maxFishDeath = UserDefault::getInstance()->getIntegerForKey(_MAX_FISHDEATH_,0);

    //load character data
    std::vector<std::string> characterName = {"monkey","frog","elephant","lion","penguin","zebra"};
    
    //play tutorial
    GameData::isPlayTutorial = UserDefault::getInstance()->getBoolForKey(_TUTORIAL_,true);
    
    for (int i = 0; i < characterName.size(); i++) {
        Character* character = new Character(characterName.at(i));
        characterArr.push_back(character);
        if (character->isSelected()) {
            GameData::characterName = character->getName();
        }
    }
    
    //Read json file path and infomation of level data
    Document levelPath;
    std::string fullPath2 = "level_data.json";
    auto fileData2 = FileUtils::getInstance()->getDataFromFile(fullPath2);
    std::string levelData((const char*)fileData2.getBytes(),fileData2.getSize());
    levelPath.Parse(levelData.c_str());
    

    auto  maxLevel = levelPath["maxlevel"].GetInt();
    for (int i = 1;i <= maxLevel ;i++) {
        LevelData* level = new LevelData(i,levelPath);
        levelDataArray.push_back(level);
    }
    
    //load game circle
    SonarCocosHelper::AmazonGameCircle::loadData("android_ids.plist");
    
}

void GameData::saveData(){
    UserDefault::getInstance()->setIntegerForKey(_BEST_SCORE_, GameData::bestScore);
    UserDefault::getInstance()->setIntegerForKey(_BEST_CAUGHT_, GameData::bestCaught);
    UserDefault::getInstance()->setIntegerForKey(_BEST_COMBO_, GameData::bestCombo);
    UserDefault::getInstance()->setIntegerForKey(_MAX_FISHDEATH_,GameData::maxFishDeath);
    UserDefault::getInstance()->setBoolForKey(_TURN_OFF_MUSIC_, GameData::isTurnOffMusic);
    UserDefault::getInstance()->setBoolForKey(_TUTORIAL_, GameData::isPlayTutorial);
}

void GameData::resetData(){
    GameData::Life = MAX_LIFE;
    GameData::Score = 0;
    GameData::currentCombo = 0;
    GameData::caught = 0;
    GameData::fishDeath = 0;
    
}

Character* GameData::getCharacterData(const std::string& name){
    for (auto charInfo : characterArr){
        if (charInfo->getName() == name) {
            return charInfo;
        }
    }
    return nullptr;
}
