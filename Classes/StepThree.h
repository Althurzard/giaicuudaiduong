//
//  StepThree.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/7/17.
//
//

#ifndef StepThree_h
#define StepThree_h
#include "StepTwo.h"

class StepThree: public StepTwo  {
private:
    int countCombo;
public:
    StepThree(){};
    ~StepThree(){};
    
    static StepThree* create(Node*);
    
    virtual void initOptions();
    virtual void addEvent();
    virtual void update(float dt);
    bool checkBubble();
};

#endif /* StepThree_h */
