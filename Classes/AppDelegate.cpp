#include "AppDelegate.h"
#include "GameMenuScene.h"
#include "BFAudioPlayer.h"
#include "SDKHelper.h"
USING_NS_CC;

static cocos2d::Size designResolutionSize = cocos2d::Size(640, 1368);
static cocos2d::Size smallResolutionSize = cocos2d::Size(480, 320);
static cocos2d::Size mediumResolutionSize = cocos2d::Size(1024, 768);
static cocos2d::Size largeResolutionSize = cocos2d::Size(2048, 1536);

AppDelegate::AppDelegate()
{
}

AppDelegate::~AppDelegate() 
{
}

// if you want a different context, modify the value of glContextAttrs
// it will affect all platforms
void AppDelegate::initGLContextAttrs()
{
    // set OpenGL context attributes: red,green,blue,alpha,depth,stencil
    GLContextAttrs glContextAttrs = {8, 8, 8, 8, 24, 8};

    GLView::setGLContextAttrs(glContextAttrs);
}

// if you want to use the package manager to install more packages,  
// don't modify or remove this function
static int register_all_packages()
{
    return 0; //flag for packages manager
}

bool AppDelegate::applicationDidFinishLaunching() {
    // initialize director
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32) || (CC_TARGET_PLATFORM == CC_PLATFORM_MAC) || (CC_TARGET_PLATFORM == CC_PLATFORM_LINUX)
        glview = GLViewImpl::createWithRect("CrushBubbleFrog", cocos2d::Rect(0, 0, designResolutionSize.width, designResolutionSize.height));
#else
        glview = GLViewImpl::create("CrushBubbleFrog");
#endif
        director->setOpenGLView(glview);
    }


    // Set the design resolution
    glview->setDesignResolutionSize(designResolutionSize.width, designResolutionSize.height, ResolutionPolicy::EXACT_FIT);
    
    std::vector<std::string> searchPaths;
    searchPaths.push_back("frog");
    searchPaths.push_back("penguin");
    searchPaths.push_back("zebra");
    searchPaths.push_back("elephant");
    searchPaths.push_back("monkey");
    searchPaths.push_back("lion");
    searchPaths.push_back("heart");
    searchPaths.push_back("timeflys");
    FileUtils::getInstance()->setSearchPaths(searchPaths);
    
    // pre-load sprite frame
    auto spriteFrameCache = SpriteFrameCache::getInstance();
    spriteFrameCache->addSpriteFramesWithFile("frog.plist", "frog.png");
    spriteFrameCache->addSpriteFramesWithFile("zebra.plist", "zebra.png");
    spriteFrameCache->addSpriteFramesWithFile("lion.plist", "lion.png");
    spriteFrameCache->addSpriteFramesWithFile("monkey.plist", "monkey.png");
    spriteFrameCache->addSpriteFramesWithFile("penguin.plist", "penguin.png");
    spriteFrameCache->addSpriteFramesWithFile("elephant.plist", "elephant.png");
    spriteFrameCache->addSpriteFramesWithFile("heart.plist", "heart.png");
    spriteFrameCache->addSpriteFramesWithFile("timeflys.plist", "timeflys.png");
    
    
    //pre-load music
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("sound/piggy_plop.wav");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("sound/background.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("sound/tincture.mp3");
    
    GameData::loadData();
    
    sdkbox::PluginOneSignal::init();
    // create a scene. it's an autorelease object
    auto scene = GameMenuScene::createScene();

    // run
    director->runWithScene(scene);

    
    //scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_SHAPE);
    return true;
}

// This function will be called when the app is inactive. Note, when receiving a phone call it is invoked.
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();

    // if you use SimpleAudioEngine, it must be paused
    BFAudioPlayer::pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
    BFAudioPlayer::resumeBackgroundMusic();
}
