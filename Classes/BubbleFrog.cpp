//
//  BubbleFrog.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/26/16.
//
//

#include "BubbleFrog.h"

BubbleFrog::BubbleFrog(){
    
}

BubbleFrog::~BubbleFrog(){
    
}

BubbleFrog* BubbleFrog::create(Layer* layer,const std::string& itemName){
    auto ret = new BubbleFrog;
    if (ret->initWithFile(fileName("BubbleNormal"))) {
        ret->autorelease();
        ret->initOptions();
        ret->initItem(itemName);
        ret->addEvent();
        layer->addChild(ret,5);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
}

BubbleFrog* BubbleFrog::create(Node* layer,const std::string& itemName){
    auto ret = new BubbleFrog;
    if (ret->initWithFile(fileName("BubbleNormal"))) {
        ret->autorelease();
        ret->initOptions();
        ret->initItem(itemName);
        ret->addEvent();
        layer->addChild(ret,5);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
}

void BubbleFrog::initItem(const std::string& itemName){
    _item = Item::create(this, itemName);
    if (itemName == "timeflys") {
        _item->setScale(0.45);
    } else if (itemName == "heart"){
        _item->setScale(0.45);
    } else {
        ParticleHelper::initParticle(this, fileName("PoisionEffect"),
                                     Vec2(this->getContentSize().width/2,this->getContentSize().height/2),
                                     1.7);
    }
    
}

void BubbleFrog::initOptions(){
    BubbleNormal::initOptions();
    //this->setName(BUBBLE_ITEM_NAME);
}

void BubbleFrog::addEvent() {
    auto listener = cocos2d::EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = [&](cocos2d::Touch *touch, cocos2d::Event *event){
        //dont consume touch when game over
        if (GameData::GameOver) {
            return false;
        }
        auto rect = this->getBoundingBox();
        if (rect.containsPoint(touch->getLocation())) {
            GameData::isTapObjects = true;
            ComboLabel::create(this->getParent(), touch->getLocation());
            this->Explode();
            return true;
        } 
        return false;
    };
    
    listener->onTouchEnded = [=](cocos2d::Touch *touch, cocos2d::Event *event) {
        GameData::isTapObjects = false;
    };
    
    
    cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
}

void BubbleFrog::changeState(float dt){
    if (timer == 7) {
        _item->Death(this->getPosition());
    }
    BubbleNormal::changeState(dt);
    
}

void BubbleFrog::Explode(){
    BFAudioPlayer::playBubbleExplode();
    this->unscheduleUpdate();
    this->_item->escape(this->getPosition());
    ParticleHelper::initParticle(this->getParent(),
                                 fileName("BubbleExplode"),
                                 this->getPosition(),
                                 2.5);
    
    this->removeFromParent();
}
