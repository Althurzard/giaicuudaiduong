//
//  StepThree.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/7/17.
//
//

#include "StepThree.h"


StepThree* StepThree::create(Node* node ){
    auto ret = new StepThree;
    if (ret->init()) {
        ret->autorelease();
        ret->initOptions();
        node->addChild(ret,10);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
    
}

void StepThree::initOptions(){
    this->setTag(Step::Three);
    GameData::tempCombo = 0;
    auto bubble1 = BubbleNormal::create(this);
    bubble1->setPosition(centerScene);
    
    auto bubble2 = BubbleNormal::create(this);
    bubble2->setPosition(bubble1->getPositionX() - bubble1->getBoundingBox().size.width/2,
                         bubble1->getBoundingBox().getMinY() - bubble1->getBoundingBox().size.height/2 - 10);
    
    auto bubble3 = BubbleNormal::create(this);
    bubble3->setPosition(bubble1->getPositionX() + bubble1->getBoundingBox().size.width/2,
                         bubble1->getBoundingBox().getMaxY() + bubble1->getBoundingBox().size.height/2 + 10);
    this->_step = Step::Three;
    this->countCombo = 0;
    StepOne::setMesseages("Chạm nhanh vào từng bong bóng để tạo thành chuỗi combo", Vec2(centerScene.x,visibleResolutionSize.height*0.15));
    this->scheduleUpdate();
}

void StepThree::update(float dt){
    if (countCombo != GameData::tempCombo) {
        cocos2d::log("%d",countCombo);
        cocos2d::log("%d",GameData::tempCombo);
        if (countCombo > GameData::tempCombo) {
            this->unscheduleUpdate();
            addEvent();
            StepOne::setMesseages("Đừng để hụt! Bạn phải chạm nhanh và chính xác hơn nữa!", centerScene);
            StepOne::addButton();
            return;
        }
        countCombo = GameData::tempCombo;
    }
    
    if (GameData::tempCombo >= 3) {
        this->unscheduleUpdate();
        cocos2d::log("next step");
        BaseTutorialScene::nextStep(Step::Four);
        this->removeFromParent();
        return;
    }
    
    if (!checkBubble()) {
        this->unscheduleUpdate();
        addEvent();
        StepOne::setMesseages("Don't let it disappeared!", centerScene);
        StepOne::addButton();
        return;
    }
}

void StepThree::addEvent(){
    StepTwo::addEvent();
}
bool StepThree::checkBubble(){
    int temp = 0;
    for (auto child : this->getChildren()){
        if (child->getName() == BUBBLE_NORMAL_NAME) {
            temp++;
        }
    }
    return temp;
}

