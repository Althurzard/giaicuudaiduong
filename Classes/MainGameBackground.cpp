//
//  MainGameBackground.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/27/16.
//
//

#include "MainGameBackground.h"
#include "MissLabel.h"

MainGameBackground::MainGameBackground(){
    
}

MainGameBackground::~MainGameBackground(){
    
}

MainGameBackground* MainGameBackground::create(Layer* layer){
    auto ret = new MainGameBackground;
    if (ret->initWithFile(fileName("BackgroundGame"))) {
        ret->autorelease();
        ret->initOptions();
        ret->addEvent();
        layer->addChild(ret,1);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
}

void MainGameBackground::initOptions(){

    auto sY = MAX(visibleResolutionSize.height / this->getContentSize().height,
                  this->getContentSize().height / visibleResolutionSize.height);
    
    auto sX = MAX(this->getContentSize().width / visibleResolutionSize.width,
                  visibleResolutionSize.width / this->getContentSize().width);
    this->setScale(sX,sY);
    this->setPosition(centerScene);
    
}

void MainGameBackground::addEvent(){
    auto listener = cocos2d::EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(false);
    

    listener->onTouchBegan = [&](cocos2d::Touch *touch, cocos2d::Event *event){
        auto rect = this->getBoundingBox();
        if (!GameData::isTapObjects && rect.containsPoint(touch->getLocation())) {
           //tap miss when having combo
            if (GameData::tempCombo != 0 ) {
                MissLabel::create(this->getParent(), touch->getLocation());
            }
            return true;
        }
        return false;
    };
    
    listener->onTouchEnded = [&](cocos2d::Touch *touch, cocos2d::Event *event) {

    };
    
    
    
    cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
}
