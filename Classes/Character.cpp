//
//  Character.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/2/17.
//
//

#include "Character.h"
#include "GameData.h"
#define UNLOCK_CHARACTER "IsUnlock"
#define SELECTED_CHARACTER "IsSelected"

Character::Character(){
    
}

Character::~Character(){
    
}

Character::Character(const std::string& name){
    this->name = name;
    
    
   
    char str[100] = {0};
    
    
   
    if (this->name == "frog") {
        sprintf(str, "%s%s",this->name.c_str(),UNLOCK_CHARACTER);
        this->unlock = UserDefault::getInstance()->getBoolForKey(str, true);
        sprintf(str, "%s%s",this->name.c_str(),SELECTED_CHARACTER);
        this->selected = UserDefault::getInstance()->getBoolForKey(str, true);
    } else {
        sprintf(str, "%s%s",this->name.c_str(),UNLOCK_CHARACTER);
        this->unlock = UserDefault::getInstance()->getBoolForKey(str,false);
        sprintf(str, "%s%s",this->name.c_str(),SELECTED_CHARACTER);
        this->selected = UserDefault::getInstance()->getBoolForKey(str, false);
    }



    
    if (name == "frog") {
        this->position = Vec2(centerScene.x - visibleResolutionSize.width * 0.3,
                              centerScene.y + visibleResolutionSize.height*0.15);
    } else if (name == "elephant") {
        this->position = Vec2(centerScene.x - visibleResolutionSize.width * 0.3,
                              centerScene.y - visibleResolutionSize.height*0.15);
    } else if (name == "monkey") {
        this->position = Vec2(centerScene.x + visibleResolutionSize.width * 0.3,
                              centerScene.y - visibleResolutionSize.height*0.15);
    } else if (name == "zebra") {
        this->position = Vec2(centerScene.x + visibleResolutionSize.width * 0.3,
                              centerScene.y + visibleResolutionSize.height*0.15);
    } else if (name == "penguin") {
        this->position = Vec2(centerScene.x,centerScene.y - visibleResolutionSize.height*0.15);
    }else if (name == "lion") {
        this->position = Vec2(centerScene.x,centerScene.y + visibleResolutionSize.height*0.15);
    } else {
        this->position = centerScene;
    }
 
}


std::string Character::getName(){
    return name;
}

bool Character::isUnlock(){
    return unlock;
}

bool Character::isSelected(){
    return selected;
}

Vec2 Character::getPosition(){
    return position;
}

void Character::setUnlock(bool unlock){
    this->unlock = unlock;
    char str[100] = {0};
    sprintf(str, "%s%s",this->name.c_str(),UNLOCK_CHARACTER);

    cocos2d::UserDefault::getInstance()->setBoolForKey(str, this->unlock);
    UserDefault::getInstance()->flush();
}

void Character::setSelect(bool select){
    this->selected = select;
    char str[100] = {0};
    sprintf(str, "%s%s",this->name.c_str(),SELECTED_CHARACTER);

    cocos2d::UserDefault::getInstance()->setBoolForKey(str, this->selected);
    UserDefault::getInstance()->flush();
}

