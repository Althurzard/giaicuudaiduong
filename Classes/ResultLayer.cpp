//
//  ResultLayer.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/30/16.
//
//

#include "ResultLayer.h"
#include "BaseGameScene.h"
#include "GameMenuScene.h"
#include "HUDLayer.h"
ResultLayer::ResultLayer(){
    
}

ResultLayer::~ResultLayer(){
    btnArray.clear();
}

ResultLayer* ResultLayer::create(Scene* scene,const GameResult& Result){
    auto ret = new ResultLayer;
    if (ret->init()) {
        ret->autorelease();
        ret->initOptions(Result);
        ret->addEvent();
        scene->addChild(ret,10);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
}

void ResultLayer::initOptions(const GameResult& Result){
    

    //show ad
    
    auto random = cocos2d::random(2, 20);
    if (random % 4 == 0) {
        SDKHelper::Admob::showFullscreen();
    }
    
    
    
    this->result = Result;
    //button
    auto scaleMin = 1.8f;
    switch (Result) {
        case Win:
            GameData::selectedLevel += 1;
            if (GameData::selectedLevel == 9) {
                GameData::selectedLevel = 0;
            }
            GameData::levelDataArray.at(GameData::selectedLevel)->setUnlock(true);
            _nextBtn = BubbleButton::create(this,
                                            scaleMin,
                                            fileName("NextLevelButton"),
                                            0.2);
            break;
        case Lose:
            _nextBtn = BubbleButton::create(this,
                                            scaleMin,
                                            fileName("PlayAgainButton"),
                                            0.2);
            break;
    }
    
    _nextBtn->setPosition(visibleResolutionSize.width*0.5,
                          visibleResolutionSize.height * 0.5 -
                          _nextBtn->getContentSize().height * 0.3);
    _nextBtn->setOpacity(0);
    
    btnArray.pushBack(_nextBtn);
    
    
    _rateBtn = BubbleButton::create(this,
                                    scaleMin,
                                    fileName("FeedbackButton"),
                                    0.2);
    
    _rateBtn->setPosition(_nextBtn->getBoundingBox().getMaxX() +
                          _rateBtn->getBoundingBox().size.width * 0.6,
                          _nextBtn->getPositionY());
    _rateBtn->setOpacity(0);
    
    btnArray.pushBack(_rateBtn);
    
    _homeBtn = BubbleButton::create(this,
                                    scaleMin,
                                    fileName("MenuButton"),
                                    0.25);
    _homeBtn->setPosition(_nextBtn->getBoundingBox().getMinX() -
                          _homeBtn->getBoundingBox().size.width *0.6,
                          _nextBtn->getPositionY());
    _homeBtn->setOpacity(0);
    
    btnArray.pushBack(_homeBtn);
    
    _highScoreBoard =  HighscoreBoard::create(this);
    
    _gameResultBoard = GameResultBoard::create(this, Result);
    

    //animation
    auto time = 1.0;
    auto move = MoveTo::create(time, Vec2(0,0));
    auto easeIn = EaseInOut::create(move, 0.5);
    
    _highScoreBoard->runAction(easeIn);
    
    _gameResultBoard->runAction(easeIn->clone());
    
    auto sequence = GameHelper::squenceScaleInOut(btnArray, time,0.3,0.2);
    
    this->runAction(sequence);
    
}

void ResultLayer::addEvent(){
    auto listener = cocos2d::EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = [&](cocos2d::Touch *touch, cocos2d::Event *event){
        auto nextRect = _nextBtn->getBoundingBox();
        auto rateRect = _rateBtn->getBoundingBox();
        auto homeRect = _homeBtn->getBoundingBox();

        if (nextRect.containsPoint(touch->getLocation())) {
            cocos2d::log("switch scene");
            GameData::GameOver = false;
            this->_nextBtn->Explode();
            this->switchScene();
            return true;
        } else if (homeRect.containsPoint(touch->getLocation())) {
            GameData::GameOver = false;
            GameData::Mode = MODE::FREE;
            this->_homeBtn->Explode();
            GameData::resetData();
            auto scene = GameMenuScene::createScene();
            
            Director::getInstance()->replaceScene(TransitionFade::create(0.5, scene));
            
            return true;
        } else if (rateRect.containsPoint(touch->getLocation())){
            BFAudioPlayer::playBubbleExplode();
            cocos2d::Application::getInstance()->openURL("http://www.bestappsforphone.com/samsunggameofthemonth");
            return true;
        }
        
        return false;
    };
    
    
    listener->onTouchEnded = [&](cocos2d::Touch *touch, cocos2d::Event *event) {
        
    };
    
    
    cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
}

void ResultLayer::switchScene(){
    
    GameData::resetData();
    auto scene = BaseGameScene::createScene();
    Director::getInstance()->replaceScene(TransitionFade::create(0.5, scene));
}
