//
//  BubbleNormal.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/26/16.
//
//

#ifndef BubbleNormal_h
#define BubbleNormal_h
#include "Bubble.h"
#include "ComboLabel.h"
#include "MissLabel.h"
#define BUBBBLE_TAG 10
#define BUBBLE_NORMAL_NAME  "BUBBLE NORMAL"

class BubbleNormal: public Bubble {
protected:
    int timer;
public:
    BubbleNormal();
    ~BubbleNormal();
    
    static BubbleNormal* create(cocos2d::Layer*);
    static BubbleNormal* create(cocos2d::Scene*);
    static BubbleNormal* create(cocos2d::Node*);
    virtual void initOptions();
    virtual void addEvent();
    virtual void Explode();
    virtual int getTimer();
    virtual void update(float dt);
    virtual void changeState(float dt);
};

#endif /* BubbleNormal_h */
