//
//  GameLevel.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/4/17.
//
//

#ifndef GameLevel_h
#define GameLevel_h
#include "BaseGameMenu.h"

class GameLevel: public BaseGameMenu {
    
public:
    GameLevel();
    ~GameLevel();
    
    static GameLevel* create(Node*);
    
    void initOptions();
    void addEvent();
    bool chooseLevel(Node*);
    void hide();
    
};

#endif /* GameLevel_h */
