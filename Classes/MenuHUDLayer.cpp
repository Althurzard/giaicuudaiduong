//
//  MenuHUDLayer.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/1/17.
//
//

#include "MenuHUDLayer.h"

MenuHUDLayer::MenuHUDLayer(){
    
}

MenuHUDLayer::~MenuHUDLayer(){
    
}

MenuHUDLayer* MenuHUDLayer::create(Scene* scene){
    auto ret = new MenuHUDLayer;
    if (ret->init()) {
        ret->autorelease();
        ret->initOptions();
        scene->addChild(ret,10);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
}

void MenuHUDLayer::initOptions(){
    
    
    if (GameData::isTurnOffMusic) {
        _musicBtn = Sprite::create(fileName("TurnOffMusic"));
    } else {
        _musicBtn = Sprite::create(fileName("TurnOnMusic"));
    }
    _musicBtn->setScale(1.1);
    _musicBtn->setPosition(visibleResolutionSize.width - _musicBtn->getBoundingBox().size.width/2 - 15,visibleResolutionSize.height*0.15);
    _musicBtn->setCascadeOpacityEnabled(true);
    this->addChild(_musicBtn);
    _musicBtn->setOpacity(0);
    
    //animation
    auto wait = DelayTime::create(0.5f);
    auto callback = CallFunc::create([&](){
        this->_musicBtn->setScale(0);
        this->_musicBtn->setOpacity(255);
        GameHelper::scaleAniamtion(this->_musicBtn, 1.3, 1.3, 1.1, 1.1, 0.15);
        this->addEvent();
    });
    this->runAction(Sequence::create(wait,callback, NULL));
    
}

void MenuHUDLayer::addEvent(){
    auto listener = cocos2d::EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = [&](cocos2d::Touch *touch, cocos2d::Event *event){
        
        auto realPosition = this->convertTouchToNodeSpace(touch);
        auto musicRect = this->_musicBtn->getBoundingBox();

        
        if (musicRect.containsPoint(realPosition)) {
            
            if (GameData::isTurnOffMusic) {
                GameData::isTurnOffMusic = false;
                this->_musicBtn->setTexture(fileName("TurnOnMusic"));
                BFAudioPlayer::playBackgroundMusic("sound/tincture.mp3");
            } else {
                GameData::isTurnOffMusic = true;
                this->_musicBtn->setTexture(fileName("TurnOffMusic"));
                BFAudioPlayer::stopBackgroundMusic();
                
            }
            return true;
        } 
        
        return false;
    };
    
    
    listener->onTouchEnded = [&](cocos2d::Touch *touch, cocos2d::Event *event) {
        
    };
    
    
    cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);

}
