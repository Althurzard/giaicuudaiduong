//
//  GreenBubble.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/29/16.
//
//

#include "GreenBubble.h"
#include "BaseGameScene.h"
GreenBubble::GreenBubble(){
    
}

GreenBubble::~GreenBubble(){
    
}

GreenBubble* GreenBubble::create(Layer* layer){
    auto ret = new GreenBubble;
    if (ret->initWithFile(fileName("BubbleNormal"))) {
        ret->autorelease();
        ret->initOptions();
        ret->addEvent();
        layer->addChild(ret,5);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
}

GreenBubble* GreenBubble::create(Node* layer){
    auto ret = new GreenBubble;
    if (ret->initWithFile(fileName("BubbleNormal"))) {
        ret->autorelease();
        ret->initOptions();
        ret->addEvent();
        layer->addChild(ret,5);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
}


void GreenBubble::initOptions(){
    BubbleNormal::initOptions();
    //this->setName(SPECIAL_GREEN_BUBBLE_NAME);
    ParticleHelper::initParticle(this,
                                 fileName("SpecialGreenBubble"),
                                 Vec2(this->getContentSize().width*0.5,
                                      this->getContentSize().height*0.5),
                                 1.7);
    
    
}

void GreenBubble::addEvent(){
    auto listener = cocos2d::EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = [&](cocos2d::Touch *touch, cocos2d::Event *event){
        //dont consume touch when game over
        if (GameData::GameOver) {
            return false;
        }
        auto rect = this->getBoundingBox();
        if (rect.containsPoint(touch->getLocation())) {
            
            auto layer = (BaseGameScene*)this->getParent();
            layer->eliminateAllNormalBubble();
            
            GameData::isTapObjects = true;
            ComboLabel::create(layer, touch->getLocation());
            this->Explode();
            return true;
        }
        return false;
    };
    
    listener->onTouchEnded = [=](cocos2d::Touch *touch, cocos2d::Event *event) {
        GameData::isTapObjects = false;
    };
    
    
    cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
}

void GreenBubble::changeState(float dt){
    BubbleNormal::changeState(dt);
}

void GreenBubble::Explode(){
    BFAudioPlayer::playBubbleExplode();
    this->unscheduleUpdate();
    
    ParticleHelper::initParticle(this->getParent(),
                                 fileName("GreenBubbleExplode"),
                                 this->getPosition(),
                                 2.5);
    this->removeFromParent();
}

