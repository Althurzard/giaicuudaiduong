//
//  MissLabel.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/27/16.
//
//

#include "MissLabel.h"
#include "ComboLabel.h"
MissLabel::MissLabel(){
    
}

MissLabel::~MissLabel(){
    
}

MissLabel* MissLabel::create(Node* layer, const Vec2& position){
    auto ret = new MissLabel;
    if (layer->getChildByName(_COMBO_NAME_)) {
        layer->removeChildByName(_COMBO_NAME_);
    }
    if (ret->initWithTTF("MISS", fileName("KarmaticArcade"), 30)) {
        ret->autorelease();
        ret->initOptions(position);
        layer->addChild(ret,3);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
    
}

void MissLabel::initOptions(const Vec2& position){
    this->setPosition(position);
    GameData::tempCombo = 0;
    auto wait = DelayTime::create(1.5);
    auto removeSelf = RemoveSelf::create();
    this->runAction(Sequence::create(wait,removeSelf, NULL));
}
