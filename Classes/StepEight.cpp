//
//  StepEight.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/7/17.
//
//

#include "StepEight.h"


StepEight* StepEight::create(Node* node){
    auto ret = new StepEight;
    if (ret->init()) {
        ret->autorelease();
        ret->initOptions();
        node->addChild(ret,10);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
}

void StepEight::initOptions(){
    //bubble can drop down
    this->setTag(Step::Eight);
    
    StepOne::setMesseages("Chạm vào bóng đồng hồ phía trên để kết thúc hướng dẫn chơi.", centerScene);
    this->_step = Step::Eight;
    
    auto arrow = Sprite::create(fileName("ArrowCurved"));
    arrow->setScale(0.3);
    arrow->setPosition(centerScene.x, visibleResolutionSize.height*0.75);
    this->addChild(arrow);
    
    auto moveUp = MoveTo::create(0.2,
                               Vec2(arrow->getPositionX(),arrow->getPositionY() + arrow->getBoundingBox().size.height));
    auto moveDown = MoveTo::create(0.2, arrow->getPosition());
    auto sequence = Sequence::create(moveUp,moveDown, NULL);
    auto repeatForver = RepeatForever::create(sequence);
    arrow->runAction(repeatForver);
    
    auto HUD = (HUDLayer*)Director::getInstance()->getRunningScene()->getChildByName(__HUD_NAME__);
    HUD->addEvent();
}
