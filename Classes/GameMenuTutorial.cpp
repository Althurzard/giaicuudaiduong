//
//  GameMenuTutorial.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/7/17.
//
//

#include "GameMenuTutorial.h"
#include "GameShop.h"
#include "GameMode.h"
#include "BaseTutorialScene.h"
GameMenuTutorial* GameMenuTutorial::create(Node* layer ){
    auto ret = new GameMenuTutorial;
    if (ret->init()) {
        ret->autorelease();
        ret->initOptions();
        layer->addChild(ret,10);
        return ret;
    }
    CC_SAFE_RETAIN(ret);
    return nullptr;
}

void GameMenuTutorial::initOptions(){
    
    bubbleLogo = Sprite::create(fileName("BubbleLogo"));
    bubbleLogo->setPosition(-bubbleLogo->getBoundingBox().size.width / 2,
                            visibleResolutionSize.height - bubbleLogo->getBoundingBox().size.height);
    bubbleLogo->setScale(2.0);
    this->addChild(bubbleLogo);
    
    
    frogLogo = Sprite::create(fileName("FrogLogo"));
    frogLogo->setPosition(visibleResolutionSize.width + frogLogo->getBoundingBox().size.width / 2,
                          bubbleLogo->getBoundingBox().getMinY() -
                          frogLogo->getBoundingBox().size.height*1.2);
    frogLogo->setScale(2.3, 2.0);
    this->addChild(frogLogo,1);
    
    _startBtn = BubbleButton::create(this, 1.6, fileName("Start"), 0.2);
    _startBtn->setPosition(centerScene.x,centerScene.y + _startBtn->getContentSize().height/2);
    _startBtn->setOpacity(0);
    btnArray.pushBack(_startBtn);
    
    
    _AboutBtn = BubbleButton::create(this, 1.6, fileName("About"), 0.2);
    _AboutBtn->setPosition(_startBtn->getPositionX() - _AboutBtn->getBoundingBox().size.width/2 - 10,
                           _startBtn->getBoundingBox().getMinY() -
                           _startBtn->getBoundingBox().size.height * 1.5);
    _AboutBtn->setOpacity(0);
    btnArray.pushBack(_AboutBtn);
    
    _moreGameBtn = BubbleButton::create(this, 1.6, fileName("MoreGame"), 0.2);
    _moreGameBtn->setPosition(_startBtn->getPositionX() + _AboutBtn->getBoundingBox().size.width/2 + 10,
                              _startBtn->getBoundingBox().getMinY() -
                              _startBtn->getBoundingBox().size.height * 1.5);
    _moreGameBtn->setOpacity(0);
    btnArray.pushBack(_moreGameBtn);
    
    auto midPoint = _startBtn->getPosition().getMidpoint(_AboutBtn->getPosition());
    
    _shopBtn = BubbleButton::create(this, 1.6, fileName("Shop"), 0.2);
    _shopBtn->setPosition(_startBtn->getBoundingBox().getMinX() -
                          _startBtn->getBoundingBox().size.width/2, midPoint.y + _shopBtn->getBoundingBox().size.height * 0.15);
    _shopBtn->setOpacity(0);
    btnArray.pushBack(_shopBtn);
    
    _btnTutorial = BubbleButton::create(this, 1.6, fileName("Tutorial"), 0.2);
    _btnTutorial->setPosition(_startBtn->getBoundingBox().getMaxX() +
                              _startBtn->getBoundingBox().size.width/2, midPoint.y + _btnTutorial->getBoundingBox().size.height * 0.15);
    _btnTutorial->setOpacity(0);
    btnArray.pushBack(_btnTutorial);
    
    
    //animation
    auto time = 0.5f;
    auto move1 = MoveTo::create(time, Vec2(bubbleLogo->getBoundingBox().size.width/2,
                                           bubbleLogo->getPositionY()));
    bubbleLogo->runAction(move1);
    
    auto move2 = MoveTo::create(time, Vec2(visibleResolutionSize.width -
                                           frogLogo->getBoundingBox().size.width/2,
                                           frogLogo->getPositionY()));
    frogLogo->runAction(move2);
    
    auto sequence = GameHelper::squenceScaleInOut(btnArray, time);
    
    this->runAction(sequence);
    
    this->addEvent();
}

void GameMenuTutorial::addEvent(){
    auto listener = cocos2d::EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = [=](cocos2d::Touch *touch, cocos2d::Event *event){
        
        auto realPosition = this->convertTouchToNodeSpace(touch);
        auto startRect = this->_startBtn->getBoundingBox();
        auto shopRect = this->_shopBtn->getBoundingBox();
        auto aboutRect = this->_AboutBtn->getBoundingBox();
        auto moreGameRect = this->_moreGameBtn->getBoundingBox();
        auto tutorialRect = this->_btnTutorial->getBoundingBox();
        if (startRect.containsPoint(realPosition)) {
            
            this->_startBtn->Explode();
            this->btnArray.eraseObject(this->_startBtn);
            this->hide();
            BaseGameMenu::goToPage(this, Page::SelectMode);
            return true;
        } else if (shopRect.containsPoint(realPosition)) {
            this->_shopBtn->Explode();
            this->btnArray.eraseObject(this->_shopBtn);
            this->hide();
            BaseGameMenu::goToPage(this, Page::Shop);
            return true;
        } else if (aboutRect.containsPoint(realPosition)) {
            BFAudioPlayer::playBubbleExplode();
            cocos2d::Application::getInstance()->openURL("http://www.bestappsforphone.com");
            return true;
        } else if (moreGameRect.containsPoint(realPosition)) {
            BFAudioPlayer::playBubbleExplode();
            cocos2d::Application::getInstance()->openURL("http://www.bestappsforphone.com/samsunggameofthemonth");
        } else if (tutorialRect.containsPoint(realPosition)) {
            this->_btnTutorial->Explode();
            auto scene = BaseTutorialScene::createScene();
            Director::getInstance()->replaceScene(TransitionFade::create(0.5, scene));
            return true;
        }
        
        return false;
    };
    
    
    listener->onTouchEnded = [&](cocos2d::Touch *touch, cocos2d::Event *event) {
        
    };
    
    
    cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
    
}


void GameMenuTutorial::hide(){
    GameMenu::hide();
}
