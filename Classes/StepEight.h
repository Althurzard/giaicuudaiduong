//
//  StepEight.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/7/17.
//
//

#ifndef StepEight_h
#define StepEight_h
#include "StepTwo.h"

class StepEight: public StepTwo {
    
public:
    StepEight(){};
    ~StepEight(){};
    
    static StepEight* create(Node*);
    
    virtual void initOptions();
    
};
#endif /* StepEight_h */
