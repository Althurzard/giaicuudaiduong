//
//  GameLevel.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/4/17.
//
//

#include "GameLevel.h"


GameLevel::GameLevel(){
    
}

GameLevel::~GameLevel(){
    
}

GameLevel* GameLevel::create(Node* layer){
    auto ret = new GameLevel;
    if (ret->init()) {
        ret->autorelease();
        ret->initOptions();
        ret->addEvent();
        layer->addChild(ret,10);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
}

void GameLevel::initOptions(){
    BaseGameMenu::initOptions();
    
    for (int i = 0; i < GameData::levelDataArray.size() ; i++) {
        auto levelData = GameData::levelDataArray.at(i);
        auto levelBtn = BubbleButton::create(this, 1.6, levelData->getPath(), 0.2);
        levelBtn->setPosition(levelData->getPosition());
        levelBtn->setOpacity(0);
        levelBtn->setTag(i);
        btnArray.pushBack(levelBtn);
        
        
    }
    
    auto wait = DelayTime::create(0.3f);
    auto callback = CallFunc::create([&](){
        
        for (auto node : this->btnArray) {
            auto tempScale = node->getScale();
            node->setScale(0);
            node->setOpacity(255);
            
            //blur bubble when player haven't unlocked level yet.
            auto level = node->getTag();
            if (level >= 0) {
                auto levelData = GameData::levelDataArray.at(level);
                if (!levelData->isUnlock()) {
                    node->setOpacity(100);
                }
            }
            
            GameHelper::scaleAniamtion(node,
                                       tempScale+0.2,
                                       tempScale+0.2,
                                       tempScale,
                                       tempScale,
                                       0.15);
        }
        
    });
    
    auto squen = Sequence::create(wait,callback, NULL);
    this->runAction(squen);
}

void GameLevel::addEvent(){
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = [&](cocos2d::Touch* touch, cocos2d::Event* event){
        
        auto realPos = this->convertTouchToNodeSpace(touch);
        auto backRect = this->_backBtn->getBoundingBox();
        
        if (backRect.containsPoint(realPos)) {
            //back to menu
            this->_backBtn->Explode();
            this->btnArray.eraseObject(this->_backBtn);
            this->hide();
            BaseGameMenu::goToPage(this, Page::SelectMode);
            return true;
        }
        
        for(auto btn : this->btnArray){
            auto btnRect = btn->getBoundingBox();
            if (btnRect.containsPoint(realPos)) {
                
                return chooseLevel(btn);
                
            }
        }
        return false;
    };
    
    cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
}

bool GameLevel::chooseLevel(Node* btn){
    GameData::selectedLevel = btn->getTag();
    
    //check if previous level is not unlock yet, then return false
    //player have to unlock previous level to play next level
    if (GameData::levelDataArray.at(GameData::selectedLevel)->isUnlock()) {
        if (typeid(*btn) == typeid(BubbleButton)) {
            //if there is a bubble, then make effect
            ((BubbleButton*)btn)->Explode();
            btnArray.eraseObject(btn);
        }
        this->hide();
        BaseGameMenu::goToPage(this, Page::MissionInfo);
        return true;
    }
        
    return false;
    
}

void GameLevel::hide() {
    BaseGameMenu::hide();
}
