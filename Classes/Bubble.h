//
//  Bubble.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/26/16.
//
//

#ifndef Bubble_h
#define Bubble_h
#include "GameData.h"
#include "PhysicTag.h"
#include "ParticleHelper.h"
#include "BFAudioPlayer.h"
class Bubble: public cocos2d::Sprite {
private:
    
public:
    Bubble(){};
    ~Bubble(){};
    
    virtual void initOptions() = 0;
    virtual void addEvent() = 0;
    virtual void Explode() = 0;
    
};

#endif /* Bubble_h */
