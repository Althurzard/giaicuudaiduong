//
//  BaseGameMenu.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/1/17.
//
//

#ifndef BaseGameMenu_h
#define BaseGameMenu_h
#include "Item.h"
#include "BubbleButton.h"
#include "GameHelper.h"

enum Page {
    Menu,
    Shop,
    SelectMode,
    SelectLevel,
    MissionInfo
};
class BaseGameMenu: public Node {
protected:
    Vector<Node*> btnArray;
    BubbleButton* _backBtn;
public:
    BaseGameMenu(){};
    ~BaseGameMenu(){
        btnArray.clear();
    };
    virtual void initOptions();
    virtual void hide();
    static void goToPage(Node* currentPage,const Page& goToPage, const float& delay = 0.3);
        
};

#endif /* BaseGameMenu_h */
