//
//  BlueBubble.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/29/16.
//
//

#include "BlueBubble.h"
#include "BaseGameScene.h"
BlueBubble::BlueBubble(){
    
}

BlueBubble::~BlueBubble(){
    
}

BlueBubble* BlueBubble::create(Layer* layer){
  
    auto ret = new BlueBubble;
    if (ret->initWithFile(fileName("BubbleNormal"))) {
        ret->autorelease();
        ret->initOptions();
        ret->addEvent();
        layer->addChild(ret,5);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
}

BlueBubble* BlueBubble::create(Node* layer){
    auto ret = new BlueBubble;
    if (ret->initWithFile(fileName("BubbleNormal"))) {
        ret->autorelease();
        ret->initOptions();
        ret->addEvent();
        layer->addChild(ret,5);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
}

void BlueBubble::initOptions(){
    BubbleNormal::initOptions();
    ParticleHelper::initParticle(this,
                                 fileName("SpecialBlueBubble"),
                                 Vec2(this->getContentSize().width*0.5,
                                      this->getContentSize().height*0.5),
                                 1.7);
    
    
}

void BlueBubble::addEvent(){
    auto listener = cocos2d::EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = [&](cocos2d::Touch *touch, cocos2d::Event *event){
        //dont consume touch when game over
        if (GameData::GameOver) {
            return false;
        }
        auto rect = this->getBoundingBox();
        if (rect.containsPoint(touch->getLocation())) {
            
            auto layer = (BaseGameScene*)this->getParent();
            //init special effect and shooting
            layer->startShooting(this->getPosition());
            GameData::isTapObjects = true;
            ComboLabel::create(layer, touch->getLocation());
            this->Explode();
            return true;
        }
        return false;
    };
    
    listener->onTouchEnded = [=](cocos2d::Touch *touch, cocos2d::Event *event) {
        GameData::isTapObjects = false;
    };
    
    
    cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
}

void BlueBubble::changeState(float dt){
    BubbleNormal::changeState(dt);
}

void BlueBubble::Explode(){

    BubbleNormal::Explode();
}
