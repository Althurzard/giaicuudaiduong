//
//  GameHelper.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/27/16.
//
//

#include "GameHelper.h"


void GameHelper::setNormalNumber(cocos2d::Node* parent,
                             const cocos2d::Vec2& desiredPosition,
                             const int& number,
                             const float scale,
                             const NumberAlignment &Align = Left){
    auto cache = cocos2d::SpriteFrameCache::getInstance();
    int totalScore = number;
    char str[100] = {0};
    
    cocos2d::Vector<cocos2d::Sprite*> _scoreNumArray;
    for (int i = 0; ; i++) {
        int temp = totalScore % 10 ;
        sprintf(str, "score_number_%d.png", temp);
        auto score = cocos2d::Sprite::createWithSpriteFrame(cache->getSpriteFrameByName(str));
        score->setScale(scale);
        _scoreNumArray.pushBack(score);
        
        totalScore /= 10;
        if (totalScore == 0) {
            break;
        }
        
    }
    
    cocos2d::Vec2 tempPosition;
    switch (Align) {
        case Left:
            tempPosition = desiredPosition;
            break;
        case Right:
            break;
        case Center:
            break;
        default:
            break;
    }
    
    switch (Align) {
        case Left:
            leftAlignment(parent,desiredPosition, _scoreNumArray);
            break;
        case Right:
            rightAlignment(parent,desiredPosition, _scoreNumArray);
            break;
        case Center:
            centerAlignment(parent,desiredPosition, _scoreNumArray);
            break;
        default:
            break;
    }
}

void GameHelper::setSpeicalNumber(cocos2d::Node* parent,
                              const cocos2d::Vec2& desiredPosition,
                              const int& number,
                              const float scale,
                              const NumberAlignment &Align = Left){
    auto cache = cocos2d::SpriteFrameCache::getInstance();
    int totalScore = number;
    char str[100] = {0};
    
    cocos2d::Vector<cocos2d::Sprite*> _scoreNumArray;
    for (int i = 0; ; i++) {
        int temp = totalScore % 10 ;
        sprintf(str, "number_%d.png", temp);
        auto score = cocos2d::Sprite::createWithSpriteFrame(cache->getSpriteFrameByName(str));
        score->setScale(scale);
        _scoreNumArray.pushBack(score);
        
        totalScore /= 10;
        if (totalScore == 0) {
            break;
        }
        
    }
    
    
    switch (Align) {
        case Left:
            leftAlignment(parent,desiredPosition, _scoreNumArray);
            break;
        case Right:
            rightAlignment(parent,desiredPosition, _scoreNumArray);
            break;
        case Center:
            centerAlignment(parent,desiredPosition, _scoreNumArray);
            break;
        default:
            break;
    }
    
    
    
}

void GameHelper::leftAlignment(cocos2d::Node* parent,
                           const cocos2d::Vec2& desiredPosition,
                           cocos2d::Vector<cocos2d::Sprite*> _scoreNumArray){
    
    for (long i = _scoreNumArray.size() - 1; i >= 0; i--) {
        if (i == _scoreNumArray.size() - 1) {
            _scoreNumArray.at(i)->setPosition(desiredPosition);
        } else {
            _scoreNumArray.at(i)->setPosition(_scoreNumArray.at(i+1)->getPositionX() + _scoreNumArray.at(i+1)->getBoundingBox().size.width * 1.1, _scoreNumArray.at(i+1)->getPositionY());
            
            
        }
        parent->addChild(_scoreNumArray.at(i));
    }
    _scoreNumArray.clear();
}
void GameHelper::rightAlignment(cocos2d::Node* parent,
                            const cocos2d::Vec2& desiredPosition,
                            cocos2d::Vector<cocos2d::Sprite*> _scoreNumArray){
    for (long i = 0; i < _scoreNumArray.size(); i++) {
        if (i == 0) {
            _scoreNumArray.at(i)->setPosition(desiredPosition);
        } else {
            _scoreNumArray.at(i)->setPosition(_scoreNumArray.at(i-1)->getPositionX() - _scoreNumArray.at(i+1)->getBoundingBox().size.width * 1.1, _scoreNumArray.at(i+1)->getPositionY());
            
            
        }
        parent->addChild(_scoreNumArray.at(i));
    }
    _scoreNumArray.clear();
}

void GameHelper::centerAlignment(cocos2d::Node* parent,
                             const cocos2d::Vec2& desiredPosition,
                             cocos2d::Vector<cocos2d::Sprite*> _scoreNumArray){
    long mid = _scoreNumArray.size() / 2;
    parent->addChild(_scoreNumArray.at(mid));
    
    if (mid != 0) {
        
        for (long before = mid - 1, after = mid + 1; ; before--,after++) {
            if (before>= 0) {
                _scoreNumArray.at(before)->setPosition(_scoreNumArray.at(before+1)->getPositionX() - _scoreNumArray.at(before+1)->getBoundingBox().size.width * 1.1, _scoreNumArray.at(before+1)->getPositionY());
                parent->addChild(_scoreNumArray.at(before));
            }
            if (after < _scoreNumArray.size()) {
                _scoreNumArray.at(after)->setPosition(_scoreNumArray.at(after-1)->getPositionX() - _scoreNumArray.at(after-1)->getBoundingBox().size.width * 1.1, _scoreNumArray.at(after-1)->getPositionY());
                parent->addChild(_scoreNumArray.at(after));
            }
        }
        
    }
    _scoreNumArray.clear();
}


Sequence* GameHelper::squenceScaleInOut(Vector<Node*> spriteArray,
                            const float& waitForCallback,
                            const float& extraScale,
                            const float& animateTime,
                            const float& opacity){
    
    auto wait = DelayTime::create(waitForCallback);
    auto callback = CallFunc::create([&,spriteArray,extraScale,animateTime,opacity](){
        
        for (auto node : spriteArray) {
            auto tempScale = node->getScale();
            node->setScale(0);
            node->setOpacity(opacity);
            GameHelper::scaleAniamtion(node,
                                       tempScale+extraScale,
                                       tempScale+extraScale,
                                       tempScale,
                                       tempScale,
                                       animateTime);
        }
        
    });
    return Sequence::create(wait,callback, NULL);
}




void GameHelper::scaleAniamtion(cocos2d::Node* parent,
                            const float& maxx1,
                            const float& maxy1,
                            const float& minx2,
                            const float& miny2,
                            const float& duration)
{
    auto scaleMax = cocos2d::ScaleTo::create(duration, maxx1,maxy1);
    auto scaleMin = cocos2d::ScaleTo::create(duration, minx2,miny2);
    auto sequence = cocos2d::Sequence::create(scaleMax,scaleMin, NULL);
    
    parent->runAction(sequence);
}



float GameHelper::getDesiredRotationTowards(const Vec2& fromPosition, const Vec2& toPosition)
{
    float newRotation = -CC_RADIANS_TO_DEGREES((fromPosition - toPosition).getAngle()) - 90;
    return newRotation;
}

void GameHelper::pauseRecursive(Node* _node, bool _pause)
{
    if (_node == NULL) {
        return;
    }
    if(_pause)
    {
        _node->pause();
    }
    else
        _node->resume();
    auto& children = _node->getChildren();
    for (size_t i = 0; i < children.size(); i++)
    {
        pauseRecursive(children.at(i), _pause);
    }
}
