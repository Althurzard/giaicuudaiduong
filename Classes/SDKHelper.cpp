//
//  SDKHelper.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/8/17.
//
//

#include "SDKHelper.h"
#include "SonarFrameworks.h"
bool SDKHelper::RevMob::isCacheRewardVideos = false;
bool SDKHelper::RevMob::isCompleteViewReward = false;

/*-----------------Admob--------------------*/
void SDKHelper::Admob::init(){
    //sdkbox::PluginAdMob::init();
}

void SDKHelper::Admob::showFullscreen(){
    //sdkbox::PluginAdMob::show("gameover");
    SonarCocosHelper::RevMob::showFullscreenAd();
}

void SDKHelper::Admob::showBanner(){
    //sdkbox::PluginAdMob::show("home");
    SonarCocosHelper::RevMob::showBannerAd();
}

void SDKHelper::Admob::hideBanner(){
    //sdkbox::PluginAdMob::hide("home");
    SonarCocosHelper::RevMob::hideBannerAd();
}

/*-----------------IAP--------------------*/

//void SDKHelper::IAP::init(){
//    sdkbox::IAP::init();
//}
//
//void SDKHelper::IAP::purchase(const std::string& product){
//    sdkbox::IAP::purchase(product);
//}
//
//void SDKHelper::IAP::restore(){
//    sdkbox::IAP::restore();
//}
//
//void SDKHelper::IAP::refresh(){
//    sdkbox::IAP::refresh();
//}
//
