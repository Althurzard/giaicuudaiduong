//
//  HighscoreBoard.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/30/16.
//
//

#include "HighscoreBoard.h"

HighscoreBoard::HighscoreBoard(){
    
}

HighscoreBoard::~HighscoreBoard(){
    
}

HighscoreBoard* HighscoreBoard::create(Node* node){
    auto ret = new HighscoreBoard;
    if (ret->init()) {
        ret->autorelease();
        ret->initOptions();
        node->addChild(ret,10);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
}

void HighscoreBoard::initOptions(){
    
    this->setPosition(visibleResolutionSize.width,0);
    if (GameData::bestScore < GameData::Score) {
        GameData::bestScore = GameData::Score;
    }
    
    if (GameData::bestCombo < GameData::currentCombo) {
        GameData::bestCombo = GameData::currentCombo;
    }
    
    if (GameData::bestCaught < GameData::caught) {
        GameData::bestCaught = GameData::caught;
    }
    
    if (GameData::maxFishDeath < GameData::fishDeath) {
        GameData::maxFishDeath = GameData::fishDeath;
    }
    
    GameData::saveData();
    
    
    
    //Score logo
    auto score = Sprite::create(fileName("ScoreLogo"));
    score->setScale(0.60);
    score->setPosition(score->getContentSize().width / 2, score->getContentSize().height);
    this->addChild(score);
    
    
    //caught logo
    auto caught = Sprite::create(fileName("Caught"));
    caught->setScale(0.55);
    caught->setAnchorPoint(Vec2(0.0,0.5));
    caught->setPosition(score->getBoundingBox().getMinX(),
                        score->getBoundingBox().getMaxY() +
                        caught->getBoundingBox().size.height);
    this->addChild(caught);
    
    
    
    //Max combo logo
    auto maxCombo = Sprite::create(fileName("MaxCombo"));
    maxCombo->setScale(0.60);
    maxCombo->setAnchorPoint(Vec2(0.0,0.5));
    maxCombo->setPosition(score->getBoundingBox().getMinX(),
                          caught->getBoundingBox().getMaxY() +
                          maxCombo->getBoundingBox().size.height);
    this->addChild(maxCombo);
    
    //Fish death logo
    auto fishdeath = Sprite::create(fileName("FishDeathLogo"));
    fishdeath->setAnchorPoint(Vec2(0.0,0.5));
    fishdeath->setScale(0.50);
    fishdeath->setPosition(score->getBoundingBox().getMinX(),
                           maxCombo->getBoundingBox().getMaxY() +
                           fishdeath->getBoundingBox().size.height);
    this->addChild(fishdeath);

    
    //max combo label
    auto maxComboLabel = Label::createWithTTF(std::to_string(GameData::bestCombo),
                                              fileName("Chalkboard"),
                                              60);
    maxComboLabel->setAlignment(TextHAlignment::LEFT);
    maxComboLabel->setPosition(maxCombo->getBoundingBox().getMaxX() +
                               maxCombo->getBoundingBox().size.width * 0.15,
                               maxCombo->getBoundingBox().getMidY());
    this->addChild(maxComboLabel);
    
    //Score label
    auto bestScore = Label::createWithTTF(std::to_string(GameData::bestScore),
                                          fileName("Chalkboard"),
                                          60);
    bestScore->setAlignment(TextHAlignment::LEFT);
    bestScore->setPosition(maxComboLabel->getPositionX(),
                           score->getBoundingBox().getMidY());
    this->addChild(bestScore);
    
    //caught label
    auto caughtLabel = Label::createWithTTF(std::to_string(GameData::bestCaught),
                                            fileName("Chalkboard"),
                                            60);
    caughtLabel->setAlignment(TextHAlignment::LEFT);
    caughtLabel->setPosition(maxComboLabel->getPositionX(),
                             caught->getBoundingBox().getMidY());
    this->addChild(caughtLabel);
    
    //fish death label
    auto lblFishDeath = Label::createWithTTF(std::to_string(GameData::maxFishDeath),
                                            fileName("Chalkboard"),
                                            60);
    lblFishDeath->setAlignment(TextHAlignment::LEFT);
    lblFishDeath->setPosition(maxComboLabel->getPositionX(),
                             fishdeath->getBoundingBox().getMidY());
    this->addChild(lblFishDeath);
    
    auto highScoreLogo = Sprite::create(fileName("Highscore"));
    highScoreLogo->setScale(0.8);
    highScoreLogo->setPosition(visibleResolutionSize.width*0.5, fishdeath->getBoundingBox().getMaxY() + highScoreLogo->getBoundingBox().size.height);
    this->addChild(highScoreLogo);
    
    
    
}
