//
//  ComboLabel.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/27/16.
//
//

#include "ComboLabel.h"
#include "HUDLayer.h"
#include "GameData.h"
ComboLabel::ComboLabel(){
    
}

ComboLabel::~ComboLabel(){
    
}

ComboLabel* ComboLabel::create(Node* layer, const Vec2& position){
    
    if (layer->getChildByName(_COMBO_NAME_)) {
        layer->removeChildByName(_COMBO_NAME_);
    }

    GameData::tempCombo++;
    auto ret = new ComboLabel;
    
    std::string combo = "COMBO X" + std::to_string(GameData::tempCombo);
    if (ret->initWithTTF(combo, fileName("KarmaticArcade"), 30)) {
        ret->autorelease();
        ret->initOptions(position);
        layer->addChild(ret,3);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
}

void ComboLabel::initOptions(const Vec2& position){
    
    this->setName(_COMBO_NAME_);
    auto hudLayer = (HUDLayer*)Director::getInstance()->getRunningScene()->getChildByName(__HUD_NAME__);
    
        hudLayer->updateScore();
    
    if (GameData::currentCombo < GameData::tempCombo) {
       
        hudLayer->updateMCombo();
    }
    
    this->setPosition(position);
    
    auto wait = DelayTime::create(2.0);
    auto lambda = CallFunc::create([&](){
        
        GameData::tempCombo = 0;
        this->removeFromParent();
    });
    this->runAction(Sequence::create(wait,lambda, NULL));
}
