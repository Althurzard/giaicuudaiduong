//
//  GameData.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 12/26/16.
//
//

#ifndef GameData_h
#define GameData_h
#include "external/json/document.h"
#include "external/json/rapidjson.h"
#include "Character.h"
#include "LevelData.h"
#include "cocos2d.h"
#include "Templete.h"
#include "SonarFrameworks.h"
using namespace cocos2d;
using namespace rapidjson;

//visible size
#define visibleResolutionSize Director::getInstance()->getOpenGLView()->getDesignResolutionSize()

//flexible scale for all platform
#define flexibleScaleMAX(sceneFrame,spriteFrame) MAX(sceneFrame.width/spriteFrame.width,sceneFrame.height/spriteFrame.height)
#define flexibleScaleMIN(sceneFrame,spriteFrame) MIN(sceneFrame.width/spriteFrame.width,sceneFrame.height/spriteFrame.height)

//center of scene
#define centerScene Vec2(visibleResolutionSize.width * 0.5, visibleResolutionSize.height * 0.5)

#define __GAME_SCENE__ "GameScene_NAME"
#define __HUD_NAME__ "HUD_NAME"
#define __GAMEMENU_NAME__ "GameMenu_NAME"
#define __TUTORIAL_SCENE__ "TUTORIAL_SCENE"

#define MAX_LIFE 3
enum MODE {
    FREE,
    CHALLENGE
};

enum GameResult{
    Win,
    Lose
};
class GameData {

public:
    static int Time;
    static int Life;
    
    static int Score;
    static int bestScore;
    
    static int tempCombo;
    static int currentCombo;
    static int bestCombo;
    
    static int caught;
    static int bestCaught;
    
    static int fishDeath;
    static int maxFishDeath;
    
    static bool isTapObjects;
    static bool GameOver;

    static void loadData();
    static void saveData();
    static void resetData();
    static Document filePath;
    
    //character
    static std::string characterName;
    static std::vector<Character*> characterArr;
    static Character* getCharacterData(const std::string& name);
    
    //Game mode
    static MODE Mode;
    
    //Level data
    static std::vector<LevelData*> levelDataArray;
    static int selectedLevel;
    
    //position when catching item
    static Vec2 HeartPosition;
    static Vec2 FrogPosition;
    static Vec2 DeathFrogPosition;
    static Vec2 TimerPosition;
    
    //music
    static bool isTurnOffMusic;
    
    //tutorial
    static bool isPlayTutorial;
    
    
    
};


#define fileName(file) GameData::filePath[file].GetString()
#define numbersTexture(file) GameData::filePath[file].GetInt()
#endif /* GameData_h */
