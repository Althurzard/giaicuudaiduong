//
//  StepSeven.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/7/17.
//
//

#include "StepSeven.h"


StepSeven* StepSeven::create(Node* node){
    auto ret = new StepSeven;
    if (ret->init()) {
        ret->autorelease();
        ret->initOptions();
        node->addChild(ret,10);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
}

void StepSeven::initOptions(){
    //bubble can drop down
    cocos2d::log("step seven");
    this->setTag(Step::Seven);
    StepOne::setMesseages("Một số vật phẩm khác hỗ trợ bạn.", Vec2(centerScene.x,centerScene.y - visibleResolutionSize.height*0.15));
    this->_step = Step::Eight;
    StepOne::addButton();
    addEvent();
}

void StepSeven::addEvent(){
    StepTwo::addEvent();
}
