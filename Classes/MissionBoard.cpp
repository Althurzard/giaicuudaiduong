//
//  MissionBoard.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/4/17.
//
//

#include "MissionBoard.h"


MissionBoard::MissionBoard(){
    
}

MissionBoard::~MissionBoard(){
    
}

MissionBoard* MissionBoard::create(Node* node){
    auto ret = new MissionBoard;
    if (ret->init()) {
        ret->autorelease();
        ret->initOptions();
        node->addChild(ret,10);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
}

void MissionBoard::initOptions(){
    
    auto levelData = GameData::levelDataArray.at(GameData::selectedLevel);
    
    auto lblMission = Label::createWithTTF("MISSION:", fileName("Chalkboard"), 55);
    lblMission->setColor(Color3B(79,238,252)); //blue color
    lblMission->setPosition(centerScene.x, centerScene.y + visibleResolutionSize.height*0.2);
    this->addChild(lblMission);
    
    auto name = "Catching " + GameData::characterName + "s:";
    auto lblCatchFrog = Label::createWithTTF(name, fileName("Chalkboard"), 48);
    lblCatchFrog->setAlignment(TextHAlignment::LEFT);
    lblCatchFrog->setPosition(centerScene.x - lblCatchFrog->getContentSize().width*0.25,
                              lblMission->getBoundingBox().getMinY() -
                              lblMission->getBoundingBox().size.height*1.2);
    this->addChild(lblCatchFrog,10);
    
    auto lblFrogNumber = Label::createWithTTF(std::to_string(levelData->getFrogs()),
                                              fileName("Chalkboard"),
                                              48);
    lblFrogNumber->setAlignment(TextHAlignment::LEFT);
    lblFrogNumber->setPosition(lblCatchFrog->getBoundingBox().getMaxX() +
                               lblFrogNumber->getContentSize().width *2,
                               lblCatchFrog->getPositionY());
    this->addChild(lblFrogNumber);
    
    
    
    auto lblMaxCombo = Label::createWithTTF("Max Combo:", fileName("Chalkboard"), 48);
    lblMaxCombo->setAlignment(TextHAlignment::LEFT);
    lblMaxCombo->setPosition(lblCatchFrog->getPositionX(),
                             lblCatchFrog->getBoundingBox().getMinY() -
                             lblCatchFrog->getBoundingBox().size.height);
    this->addChild(lblMaxCombo,10);
    
    auto lblComboNumber = Label::createWithTTF(std::to_string(levelData->getCombo()),
                                              fileName("Chalkboard"),
                                              48);
    lblComboNumber->setAlignment(TextHAlignment::LEFT);
    lblComboNumber->setPosition(lblFrogNumber->getPositionX(),
                               lblMaxCombo->getPositionY());
    this->addChild(lblComboNumber);
    
    
    
    auto lblTime = Label::createWithTTF("Limited time:", fileName("Chalkboard"), 48);
    lblTime->setAlignment(TextHAlignment::LEFT);
    lblTime->setPosition(lblCatchFrog->getPositionX(),
                         lblMaxCombo->getBoundingBox().getMinY() -
                         lblMaxCombo->getBoundingBox().size.height);
    this->addChild(lblTime,10);
    
    auto lblTimeNumber = Label::createWithTTF(std::to_string(levelData->getTime()),
                                               fileName("Chalkboard"),
                                               48);
    lblTimeNumber->setAlignment(TextHAlignment::LEFT);
    lblTimeNumber->setPosition(lblComboNumber->getPositionX(),
                                lblTime->getPositionY());
    this->addChild(lblTimeNumber);
    
    this->setPosition(0, visibleResolutionSize.height);
}
