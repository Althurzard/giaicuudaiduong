//
//  StepFour.cpp
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/7/17.
//
//

#include "StepFour.h"

StepFour* StepFour::create(Node* node){
    auto ret = new StepFour;
    if (ret->init()) {
        ret->autorelease();
        ret->initOptions();
        node->addChild(ret,10);
        return ret;
    }
    CC_SAFE_RELEASE(ret);
    return nullptr;
}

void StepFour::initOptions(){
    this->setTag(Step::Four);
    auto bubble = BubbleFrog::create(this, GameData::characterName);
    bubble->setPosition(centerScene);
    
    StepOne::setMesseages("Chạm nhanh vào bong bóng và bắt các chú cá lại.", Vec2(centerScene.x,visibleResolutionSize.height*0.15));
    this->_step = Step::Four;
    tempFrogs = 0;
    scheduleUpdate();
}

void StepFour::update(float dt){
    if (tempFrogs < GameData::caught) {
        this->unscheduleUpdate();
        StepOne::setMesseages("TỐT LẮM!", centerScene);
        StepOne::setMesseages("CHÚ Ý: Nếu bạn để các chú cá nhiễm độc quá lâu và không giải cứu chúng bạn sẽ bị mất 1 tim.",
                              Vec2(centerScene.x,centerScene.y - visibleResolutionSize.height*0.1),
                              false);
        this->_step = Step::Five;
        StepOne::addButton();
        addEvent();
        return;
    }
    if (checkItem()) {
        this->unscheduleUpdate();
        addEvent();
        StepOne::setMesseages("Hmm..Đừng để chúng bị nhiễm độc và rớt mất!",
                              centerScene);
        StepOne::addButton();
    }
}

bool StepFour::checkItem(){
    //frog escaped
    if (getChildByName(BUBBLE_NORMAL_NAME) == nullptr &&
        getChildByName(GameData::characterName) == nullptr) {
        return true;
    }
    return false;
}

void StepFour::addEvent(){
    StepTwo::addEvent();
}


