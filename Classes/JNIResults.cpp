//
//  JNIResults.cpp
//  RescueGiant
//
//  Created by AppleProgrammer on 03.06.15.
//
//

#include "JNIResults.h"
#include "SDKHelper.h"
#include "GameShop.h"
#if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
using namespace SonarCocosHelper;
JNIEXPORT void JNICALL Java_sonar_systems_frameworks_Vungle_VungleAds_rewardedVideoWasViewedVungle(JNIEnv* env, jclass thiz, jboolean result)
{
    //CPP code here
    CCLOG("VideoAdFinished1");
    
}

JNIEXPORT void JNICALL Java_sonar_systems_frameworks_AdColony_AdColonyAds_rewardedVideoWasViewedAdcolony(JNIEnv* env, jclass thiz, jboolean result)
{
//    //CPP code here
//    CCLOG("VideoAdFinished1");
}

JNIEXPORT void JNICALL Java_sonar_systems_frameworks_AdMob_AdMobAds_FullscreenAdPreloaded(JNIEnv* env, jclass thiz, jboolean result)
{
    //CPP code here
    //AdMob::showPreLoadedFullscreenAd();
}

//JNIEXPORT void JNICALL Java_sonar_systems_frameworks_ChartBoost_ChartBoostAds_rewardVideowasViewedChartboost(JNIEnv* env, jclass thiz, jboolean result)
//{
//
////    //CPP code here
//    SDKHelper::Chartboost::isCompleteViewReward = true;
//    auto gameShop = (GameShop*)Director::getInstance()->getRunningScene()->getChildByName(__GAMEMENU_NAME__)->getChildByName(GAME_SHOP);
//    gameShop->boughtBubble(gameShop->characterSelect);
//    
//}
//
//JNIEXPORT void JNICALL Java_sonar_systems_frameworks_ChartBoost_ChartBoostAds_FullscreenAdPreloaded(JNIEnv* env, jclass thiz, jboolean result)
//{
////    GameData::isLoadFullscreen = true;
////    //CPP code here
////    //Chartboost::showFullscreenAd();
//}
//JNIEXPORT void JNICALL Java_sonar_systems_frameworks_ChartBoost_ChartBoostAds_didCacheRewardVideo(JNIEnv* env, jclass thiz, jboolean result)
//{
//    SDKHelper::Chartboost::isCacheRewardVideos = true;
//
//}
//JNIEXPORT void JNICALL Java_sonar_systems_frameworks_ChartBoost_ChartBoostAds_didDismissRewardVideo(JNIEnv* env, jclass thiz, jboolean result)
//{
//    SonarCocosHelper::Chartboost::preLoadVideoAd();
//    
//    if (SDKHelper::Chartboost::isCompleteViewReward) {
//        SDKHelper::Chartboost::isCompleteViewReward = false;
//    } else {
//        
//    }
//    
//}

JNIEXPORT void JNICALL Java_sonar_systems_frameworks_RevMob_RevMobAds_didCacheRewardVideo(JNIEnv* env, jclass thiz, jboolean result)
{
    SDKHelper::RevMob::isCacheRewardVideos = true;
    
    
}

JNIEXPORT void JNICALL Java_sonar_systems_frameworks_RevMob_RevMobAds_didCompleteRewardVideo(JNIEnv* env, jclass thiz, jboolean result)
{
    SDKHelper::RevMob::isCompleteViewReward = true;
    auto gameShop = (GameShop*)Director::getInstance()->getRunningScene()->getChildByName(__GAMEMENU_NAME__)->getChildByName(GAME_SHOP);
    gameShop->boughtBubble(gameShop->characterSelect);
}

#endif
