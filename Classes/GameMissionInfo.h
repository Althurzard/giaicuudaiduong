//
//  GameMissionInfo.h
//  CrushBubbleFrog
//
//  Created by Nguyen Quoc Vuong on 1/4/17.
//
//

#ifndef GameMissionInfo_h
#define GameMissionInfo_h
#include "BaseGameMenu.h"
#include "MissionBoard.h"
class GameMissionInfo: public BaseGameMenu {
private:
    BubbleButton* _btnGo;
public:
    GameMissionInfo();
    ~GameMissionInfo();
    
    static GameMissionInfo* create(Node*);
    
    void initOptions();
    void addEvent();
    void hide();
    
};

#endif /* GameMissionInfo_h */
