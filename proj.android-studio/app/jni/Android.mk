LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d)
$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d/external)
$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d/cocos)
$(call import-add-path,$(LOCAL_PATH)/../../../cocos2d/cocos/audio/include)
$(call import-add-path, $(LOCAL_PATH))

LOCAL_MODULE := MyGame_shared

LOCAL_MODULE_FILENAME := libMyGame

LOCAL_SRC_FILES := hellocpp/main.cpp \
../../../Classes/AppDelegate.cpp \
../../../Classes/BaseGameMenu.cpp \
../../../Classes/BaseGameScene.cpp \
../../../Classes/BaseTutorialScene.cpp \
../../../Classes/BFAudioPlayer.cpp \
../../../Classes/BlueBubble.cpp \
../../../Classes/BubbleButton.cpp \
../../../Classes/BubbleFrog.cpp \
../../../Classes/BubbleNormal.cpp \
../../../Classes/Character.cpp \
../../../Classes/ComboLabel.cpp \
../../../Classes/GameData.cpp \
../../../Classes/GameHelper.cpp \
../../../Classes/GameLevel.cpp \
../../../Classes/GameMenu.cpp \
../../../Classes/GameMenuScene.cpp \
../../../Classes/GameMenuTutorial.cpp \
../../../Classes/GameMissionInfo.cpp \
../../../Classes/GameMode.cpp \
../../../Classes/GameResultBoard.cpp \
../../../Classes/GameShop.cpp \
../../../Classes/GreenBubble.cpp \
../../../Classes/HighscoreBoard.cpp \
../../../Classes/HUDLayer.cpp \
../../../Classes/Item.cpp \
../../../Classes/LevelData.cpp \
../../../Classes/MainGameBackground.cpp \
../../../Classes/MenuHUDLayer.cpp \
../../../Classes/MissionBoard.cpp \
../../../Classes/MissLabel.cpp \
../../../Classes/ParticleHelper.cpp \
../../../Classes/PauseLayer.cpp \
../../../Classes/ResultLayer.cpp \
../../../Classes/RandomFishNode.cpp \
../../../Classes/SDKHelper.cpp \
../../../Classes/StepEight.cpp \
../../../Classes/StepFive.cpp \
../../../Classes/StepFour.cpp \
../../../Classes/StepOne.cpp \
../../../Classes/StepSeven.cpp \
../../../Classes/StepSix.cpp \
../../../Classes/StepThree.cpp \
../../../Classes/StepTwo.cpp \
../../../Classes/TimerLabel.cpp \
../../../Classes/SonarFrameworks.cpp \
../../../Classes/JNIHelpers.cpp \
../../../Classes/JNIResults.cpp \
../../../Classes/InfoScene.cpp

LOCAL_CPPFLAGS := -DSDKBOX_ENABLED
LOCAL_LDLIBS := -landroid \
-llog
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../../Classes
LOCAL_WHOLE_STATIC_LIBRARIES := PluginIAP \
sdkbox \
android_native_app_glue \
PluginOneSignal

# _COCOS_HEADER_ANDROID_BEGIN
# _COCOS_HEADER_ANDROID_END


LOCAL_STATIC_LIBRARIES := cocos2dx_static

# _COCOS_LIB_ANDROID_BEGIN
# _COCOS_LIB_ANDROID_END

include $(BUILD_SHARED_LIBRARY)

$(call import-module,.)
$(call import-module, ./sdkbox)
$(call import-module, ./pluginiap)
$(call import-module, ./pluginonesignal)

# _COCOS_LIB_IMPORT_ANDROID_BEGIN
# _COCOS_LIB_IMPORT_ANDROID_END
